import time

start = time.time()

file = open("C:/tmp/names.csv", "r")

names = {}

for line in file:
    splitted = line.strip().split(",")
    if splitted[0] == "Id":
        continue
        
    name = splitted[1]
    count = int(splitted[5])
    
    if name in names:
        names[name] = names[name] + count
    else:
        names[name] = count


max_occurences = 0
name = ""

for key, value in names.items():
    if max_occurences < value:
        max_occurences = value
        name = key
        
print(name)
print(max_occurences)

end = time.time()
print(end - start)
