print("Versuch 1")

students = ["Max", "Monika", "Erik", "Franziska", "Monika", "Monika"]
schueler = students
print(students)
print(schueler)
students = [ i for i in students if i!="Monika" ]
print(students)
print(schueler)

#######################################
print("Versuch 2")


s1 = ["Max", "Monika", "Erik", "Franziska", "Monika", "Monika"]
s2 = s1


def remove_val(liste, value):
    liste = [ i for i in liste if i!= value ]
    return liste

s1 = remove_val(s1, "Monika")
print(s1 is s2)
print(s1)
print(s2)

######################################

print("Versuch 3")

stud1 = ["Max", "Monika", "Erik", "Franziska", "Monika", "Monika"]
stud2 = stud1

def removeall_inplace(x, l):
    for _ in range(l.count(x)):
        l.remove(x)

removeall_inplace("Monika", stud1)
print(stud1 is stud2)

print(stud2.count(0))
print(stud1)
print(stud2)


##########################################
my_list = ["Max", "Monika", "Erik", "Franziska", "Monika", "Monika"]


for item in my_list[::-1]:
    if item == "Monika":
        my_list.remove(item)

print(my_list)