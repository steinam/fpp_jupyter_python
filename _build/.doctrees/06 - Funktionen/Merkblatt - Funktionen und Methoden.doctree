����      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h� Merkblatt: Funktionen & Methoden�h]�h	�Text���� Merkblatt: Funktionen & Methoden�����}�(h� Merkblatt: Funktionen & Methoden��parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�M'�source��aC:\Schule\unterricht\fpp_jupyter_python\06 - Funktionen\Merkblatt - Funktionen und Methoden.ipynb�hhubh)��}�(hhh]�(h)��}�(h�
Funktionen�h]�h�
Funktionen�����}�(hh2hh0ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*M'h+h,hh-ubh	�	paragraph���)��}�(hXX  Funktionen sind zusammengefasste Codeblöcke. Mittels Funktionen können wir es vermeiden, mehrmals verwendete Codeblöcke zu wiederholen. Wir definieren stattdessen einmal eine Funktion, die diese Codeblöcke enthält und brauchen an weiteren Stellen nur noch (kurz) die Funktion aufzurufen, ohne die in ihr enthaltenen Codezeilen zu kopieren.�h]�hXX  Funktionen sind zusammengefasste Codeblöcke. Mittels Funktionen können wir es vermeiden, mehrmals verwendete Codeblöcke zu wiederholen. Wir definieren stattdessen einmal eine Funktion, die diese Codeblöcke enthält und brauchen an weiteren Stellen nur noch (kurz) die Funktion aufzurufen, ohne die in ihr enthaltenen Codezeilen zu kopieren.�����}�(hhBhh@hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*M'h+h,hh-hhubh)��}�(hhh]�(h)��}�(h�%Eine Funktion definieren und aufrufen�h]�h�%Eine Funktion definieren und aufrufen�����}�(hhShhQubah}�(h]�h!]�h#]�h%]�h']�uh)hh*M"Nh+h,hhNubh?)��}�(h��Wir haben schon einige Funktionen kennengelernt, die uns Python zur Verfügung stellt. Die Funktion, die wir bislang wohl am häufigsten verwendet haben, ist die print-Funktion:�h]�h��Wir haben schon einige Funktionen kennengelernt, die uns Python zur Verfügung stellt. Die Funktion, die wir bislang wohl am häufigsten verwendet haben, ist die print-Funktion:�����}�(h��Wir haben schon einige Funktionen kennengelernt, die uns Python zur Verfügung stellt. Die Funktion, die wir bislang wohl am häufigsten verwendet haben, ist die print-Funktion:�hh_hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*M#Nh+h,hhNhhub�myst_nb.nodes��CellNode���)��}�(hhh]�(hn�CellInputNode���)��}�(hhh]�h	�literal_block���)��}�(h�print("HALLO WELT")�h]�h�print("HALLO WELT")�����}�(hhhh{ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��language��ipython3�uh)hyhhvhhh+h,h*K ubah}�(h]�h!]��
cell_input�ah#]�h%]�h']�uh)hth*M2uh+h,hhqhhubhn�CellOutputNode���)��}�(hhh]�hn�CellOutputBundleNode���)��}�(�_outputs�]��nbformat.notebooknode��NotebookNode���)��(�name��stdout��output_type��stream��text��HALLO WELT
�u}��	_allownew��sba�	_renderer��default��	_metadata�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*M2uh+h,hh�hhubah}�(h]�h!]��cell_output�ah#]�h%]�h']�uh)h�hhqhhh+h,h*K ubeh}�(h]�h!]��cell�ah#]�h%]�h']��	cell_type��code�uh)hohhNhhh+h,h*K ubh?)��}�(h��Wenn wir eine eigene Funktion verwenden wollen, müssen wir sie zuerst definieren. Eine solche Funktionsdefinition hat die allgemeine Syntax:�h]�h��Wenn wir eine eigene Funktion verwenden wollen, müssen wir sie zuerst definieren. Eine solche Funktionsdefinition hat die allgemeine Syntax:�����}�(h��Wenn wir eine eigene Funktion verwenden wollen, müssen wir sie zuerst definieren. Eine solche Funktionsdefinition hat die allgemeine Syntax:�hh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*MB�h+h,hhNhhubh?)��}�(h�J**def Funktionname():**
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **Code**�h]�(hh����}�(hhhh�hhh+Nh*Nubh	�strong���)��}�(h�def Funktionname():�h]�h�def Funktionname():�����}�(h�def Funktionname():�hh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*MD�h+h,hh�hhubhh����}�(hhhh�hhh+h,h*K ubh�
�����}�(hhhh�hhh+Nh*Nubh	�raw���)��}�(h�<br>�h]�h�<br>�����}�(hhhh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']��format��html�h�h�uh)h�h+h,h*MD�hh�hhubh�
�����}�(hhhh�hhh+h,h*K ubh�       �����}�(h�       �hh�hhh+Nh*Nubh�)��}�(h�Code�h]�h�Code�����}�(h�Code�hj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*MD�h+h,hh�hhubhh����}�(hhhh�hhh+h,h*K ubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*MD�h+h,hhNhhubhp)��}�(hhh]�hu)��}�(hhh]�hz)��}�(h�Ddef multi_print():
    print("Hallo Welt!")
    print("Hallo Welt!")�h]�h�Ddef multi_print():
    print("Hallo Welt!")
    print("Hallo Welt!")�����}�(hhhj/  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj,  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*MR�h+h,hj)  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohhNhhh+h,h*K ubh?)��}�(h�UUm eine Funktion auszuführen, die definiert wurde, schreiben wir: **Funktionname()**�h]�(h�CUm eine Funktion auszuführen, die definiert wurde, schreiben wir: �����}�(h�CUm eine Funktion auszuführen, die definiert wurde, schreiben wir: �hjK  hhh+Nh*Nubh�)��}�(h�Funktionname()�h]�h�Funktionname()�����}�(h�Funktionname()�hjT  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*Mb�h+h,hjK  hhubhh����}�(hhhjK  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*Mb�h+h,hhNhhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�multi_print()�h]�h�multi_print()�����}�(hhhjr  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhjo  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*Jr h+h,hjl  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��Hallo Welt!
Hallo Welt!
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*Jr h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hjl  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohhNhhh+h,h*K ubeh}�(h]��%eine-funktion-definieren-und-aufrufen�ah!]�h#]��%eine funktion definieren und aufrufen�ah%]�h']�uh)h
h*M"Nh+h,hh-hhubh)��}�(hhh]�(h)��}�(h�Funktionen mit einem Argument�h]�h�Funktionen mit einem Argument�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J�8 h+h,hj�  ubh?)��}�(h�sMan kann Funktionen ein **Argument** übergeben, d.h. einen Wert, von dem der Code innerhalb der Funktion abhängt.�h]�(h�Man kann Funktionen ein �����}�(h�Man kann Funktionen ein �hj�  hhh+Nh*Nubh�)��}�(h�Argument�h]�h�Argument�����}�(h�Argument�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J�8 h+h,hj�  hhubh�O übergeben, d.h. einen Wert, von dem der Code innerhalb der Funktion abhängt.�����}�(h�O übergeben, d.h. einen Wert, von dem der Code innerhalb der Funktion abhängt.�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�8 h+h,hj�  hhubh?)��}�(h��**def Funktionsname(Argument):**
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **Code in dem mit dem spezifischen Argument gearbeitet wird**�h]�(hh����}�(hhhj�  hhh+Nh*Nubh�)��}�(h�def Funktionsname(Argument):�h]�h�def Funktionsname(Argument):�����}�(h�def Funktionsname(Argument):�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J�8 h+h,hj�  hhubhh����}�(hhhj�  hhh+h,h*K ubh�
�����}�(hhhj�  hhh+Nh*Nubh�)��}�(h�<br>�h]�h�<br>�����}�(hhhj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']��format�j  h�h�uh)h�h+h,h*J�8 hj�  hhubh�
�����}�(hhhj�  hhh+h,h*K ubh�       �����}�(h�       �hj�  hhh+Nh*Nubh�)��}�(h�9Code in dem mit dem spezifischen Argument gearbeitet wird�h]�h�9Code in dem mit dem spezifischen Argument gearbeitet wird�����}�(h�9Code in dem mit dem spezifischen Argument gearbeitet wird�hj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J�8 h+h,hj�  hhubhh����}�(hhhj�  hhh+h,h*K ubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�8 h+h,hj�  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�gdef multi_print2(name):
    print(name)
    print(name)
    
multi_print2("HALLO")
multi_print2("WELT")�h]�h�gdef multi_print2(name):
    print(name)
    print(name)
    
multi_print2("HALLO")
multi_print2("WELT")�����}�(hhhj8  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj5  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�_ h+h,hj2  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��HALLO
HALLO
WELT
WELT
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�_ h+h,hjL  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj2  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubh?)��}�(h��Du kannst dir einen solchen Parameter als eine zu einer Funktion gehörige Variable vorstellen. Vermeide es, einen Funktionsparameter wie eine bereits bestehende Variable zu benennen - Verwirrungsgefahr!�h]�h��Du kannst dir einen solchen Parameter als eine zu einer Funktion gehörige Variable vorstellen. Vermeide es, einen Funktionsparameter wie eine bereits bestehende Variable zu benennen - Verwirrungsgefahr!�����}�(h��Du kannst dir einen solchen Parameter als eine zu einer Funktion gehörige Variable vorstellen. Vermeide es, einen Funktionsparameter wie eine bereits bestehende Variable zu benennen - Verwirrungsgefahr!�hjo  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�� h+h,hj�  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h��name = "MARS"

def multi_print2(name):
    print(name)
    print(name)
    
multi_print2("HALLO")
multi_print2("WELT")

print(name)�h]�h��name = "MARS"

def multi_print2(name):
    print(name)
    print(name)
    
multi_print2("HALLO")
multi_print2("WELT")

print(name)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�� h+h,hj~  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��HALLO
HALLO
WELT
WELT
MARS
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj~  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubh?)��}�(h�fDu siehst, dass der Wert der Variable _name_ keinen Einfluss auf das Argument _name_ der Funktion hat!�h]�(h�&Du siehst, dass der Wert der Variable �����}�(h�&Du siehst, dass der Wert der Variable �hj�  hhh+Nh*Nubh	�emphasis���)��}�(h�name�h]�h�name�����}�(h�name�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)j�  h*J�� h+h,hj�  hhubh�" keinen Einfluss auf das Argument �����}�(h�" keinen Einfluss auf das Argument �hj�  hhh+Nh*Nubj�  )��}�(h�name�h]�h�name�����}�(h�name�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)j�  h*J�� h+h,hj�  hhubh� der Funktion hat!�����}�(h� der Funktion hat!�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�� h+h,hj�  hhubeh}�(h]��funktionen-mit-einem-argument�ah!]�h#]��funktionen mit einem argument�ah%]�h']�uh)h
h*J�8 h+h,hh-hhubh)��}�(hhh]�(h)��}�(h�Weitere Funktionen in Python�h]�h�Weitere Funktionen in Python�����}�(hj  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J�� h+h,hj�  ubh?)��}�(h�6Auch die len-Funktion für Listen kennst du schon. :-)�h]�h�6Auch die len-Funktion für Listen kennst du schon. :-)�����}�(h�6Auch die len-Funktion für Listen kennst du schon. :-)�hj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�" h+h,hj�  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(len(["Hallo", "Welt"]))�h]�h�print(len(["Hallo", "Welt"]))�����}�(hhhj"  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�I h+h,hj  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��2
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�I h+h,hj6  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubh?)��}�(h�5Du kannst die len-Funktion auch auf Strings anwenden.�h]�h�5Du kannst die len-Funktion auch auf Strings anwenden.�����}�(h�5Du kannst die len-Funktion auch auf Strings anwenden.�hjY  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*Jq h+h,hj�  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(len("Hallo"))�h]�h�print(len("Hallo"))�����}�(hhhjn  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhjk  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J� h+h,hjh  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��5
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hjh  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubh?)��}�(h�lEine Übersicht über Funktionen in Python findest du hier: https://docs.python.org/3/library/functions.html�h]�(h�<Eine Übersicht über Funktionen in Python findest du hier: �����}�(h�<Eine Übersicht über Funktionen in Python findest du hier: �hj�  hhh+Nh*Nubh	�	reference���)��}�(h�0https://docs.python.org/3/library/functions.html�h]�h�0https://docs.python.org/3/library/functions.html�����}�(h�0https://docs.python.org/3/library/functions.html�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']��refuri��0https://docs.python.org/3/library/functions.html�uh)j�  h*J"� h+h,hj�  hhubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J"� h+h,hj�  hhubeh}�(h]��weitere-funktionen-in-python�ah!]�h#]��weitere funktionen in python�ah%]�h']�uh)h
h*J�� h+h,hh-hhubh)��}�(hhh]�(h)��}�(h�"Funktionen mit mehreren Argumenten�h]�h�"Funktionen mit mehreren Argumenten�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J2� h+h,hj�  ubh?)��}�(h�4Eine Funktion darf auch mehrere Argumente enthalten.�h]�h�4Eine Funktion darf auch mehrere Argumente enthalten.�����}�(hj�  hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*J4� h+h,hj�  hhubh?)��}�(h��**def Funktionenname(Argument1, Argument2, ...):**
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **Code in dem mit Argument1, Argument2,... gearbeitet wird**�h]�(hh����}�(hhhj�  hhh+Nh*Nubh�)��}�(h�.def Funktionenname(Argument1, Argument2, ...):�h]�h�.def Funktionenname(Argument1, Argument2, …):�����}�(h�.def Funktionenname(Argument1, Argument2, ...):�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J6� h+h,hj�  hhubhh����}�(hhhj�  hhh+h,h*K ubh�
�����}�(hhhj�  hhh+Nh*Nubh�)��}�(h�<br>�h]�h�<br>�����}�(hhhj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']��format�j  h�h�uh)h�h+h,h*J6� hj�  hhubh�
�����}�(hhhj�  hhh+h,h*K ubh�       �����}�(h�       �hj�  hhh+Nh*Nubh�)��}�(h�8Code in dem mit Argument1, Argument2,... gearbeitet wird�h]�h�8Code in dem mit Argument1, Argument2,… gearbeitet wird�����}�(h�8Code in dem mit Argument1, Argument2,... gearbeitet wird�hj#  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J6� h+h,hj�  hhubhh����}�(hhhj�  hhh+h,h*K ubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J6� h+h,hj�  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�qdef multi_print(name, count):
    for i in range(0, count):
        print(name)
        
multi_print("Hallo!", 5)�h]�h�qdef multi_print(name, count):
    for i in range(0, count):
        print(name)
        
multi_print("Hallo!", 5)�����}�(hhhjA  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj>  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*JB h+h,hj;  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��#Hallo!
Hallo!
Hallo!
Hallo!
Hallo!
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*JB h+h,hjU  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj;  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubeh}�(h]��"funktionen-mit-mehreren-argumenten�ah!]�h#]��"funktionen mit mehreren argumenten�ah%]�h']�uh)h
h*J2� h+h,hh-hhubh)��}�(hhh]�(h)��}�(h�Funktionen in Funktionen�h]�h�Funktionen in Funktionen�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*JR4 h+h,hj�  ubh?)��}�(h�7Funktionen können auch ineinander geschachtelt werden:�h]�h�7Funktionen können auch ineinander geschachtelt werden:�����}�(h�7Funktionen können auch ineinander geschachtelt werden:�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*JS4 h+h,hj�  hhubhp)��}�(hhh]�hu)��}�(hhh]�hz)��}�(h�Pdef weitere_funktion():
    multi_print("Hallo!", 3)
    multi_print("Welt!", 3)�h]�h�Pdef weitere_funktion():
    multi_print("Hallo!", 3)
    multi_print("Welt!", 3)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*Jb[ h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�weitere_funktion()�h]�h�weitere_funktion()�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*Jr� h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��'Hallo!
Hallo!
Hallo!
Welt!
Welt!
Welt!
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*Jr� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubeh}�(h]��funktionen-in-funktionen�ah!]�h#]��funktionen in funktionen�ah%]�h']�uh)h
h*JR4 h+h,hh-hhubh)��}�(hhh]�(h)��}�(h�Einen Wert zurückgeben�h]�h�Einen Wert zurückgeben�����}�(hj  hj
  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J�� h+h,hj  ubh?)��}�(h��Bislang führen wir mit Funktionen einen Codeblock aus, der von Argumenten abhängen kann. Funktionen können aber auch mittels des Befehls **return** Werte zurückgeben:�h]�(h��Bislang führen wir mit Funktionen einen Codeblock aus, der von Argumenten abhängen kann. Funktionen können aber auch mittels des Befehls �����}�(h��Bislang führen wir mit Funktionen einen Codeblock aus, der von Argumenten abhängen kann. Funktionen können aber auch mittels des Befehls �hj  hhh+Nh*Nubh�)��}�(h�return�h]�h�return�����}�(h�return�hj!  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J�� h+h,hj  hhubh� Werte zurückgeben:�����}�(h� Werte zurückgeben:�hj  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�� h+h,hj  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�Fdef return_element(name):
    return name

print(return_element("Hi"))�h]�h�Fdef return_element(name):
    return name

print(return_element("Hi"))�����}�(hhhjA  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj>  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�� h+h,hj;  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��Hi
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�� h+h,hjU  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj;  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj  hhh+h,h*K ubh?)��}�(h�FSolche Funktionen mit return können wir dann wie Variablen behandeln:�h]�h�FSolche Funktionen mit return können wir dann wie Variablen behandeln:�����}�(h�FSolche Funktionen mit return können wir dann wie Variablen behandeln:�hjx  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*J�� h+h,hj  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h��def return_with_exclamation(name):
    return name + "!"

if return_with_exclamation("Hi") == "Hi!":
    print("Right!")
else:
    print("Wrong.")�h]�h��def return_with_exclamation(name):
    return name + "!"

if return_with_exclamation("Hi") == "Hi!":
    print("Right!")
else:
    print("Wrong.")�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J� h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��Right!
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�rdef maximum(a, b):
    if a < b:
        return b
    else:
        return a

result = maximum(4, 5)
print(result)�h]�h�rdef maximum(a, b):
    if a < b:
        return b
    else:
        return a

result = maximum(4, 5)
print(result)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�E h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��5
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�E h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj  hhh+h,h*K ubeh}�(h]��einen-wert-zuruckgeben�ah!]�h#]��einen wert zurückgeben�ah%]�h']�uh)h
h*J�� h+h,hh-hhubeh}�(h]��
funktionen�ah!]�h#]�h%]��
funktionen�ah']�uh)h
h*M'h+h,hhhh�
referenced�Kubeh}�(h]��merkblatt-funktionen-methoden�ah!]�h#]�� merkblatt: funktionen & methoden�ah%]�h']�uh)h
h*M'h+h,hhhhubh)��}�(hhh]�(h)��}�(h�Funktionen vs. Methoden�h]�h�Funktionen vs. Methoden�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J�l h+h,hj  ubh)��}�(hhh]�(h)��}�(h�
Funktionen�h]�h�
Funktionen�����}�(hj0  hj.  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J� h+h,hj+  ubh?)��}�(h��Bei ihrem Aufruf stehen Funktionen "für sich" und das, worauf sie sich beziehen steht ggf. als Argument in den Klammern hinter ihnen:�h]�h��Bei ihrem Aufruf stehen Funktionen “für sich” und das, worauf sie sich beziehen steht ggf. als Argument in den Klammern hinter ihnen:�����}�(h��Bei ihrem Aufruf stehen Funktionen "für sich" und das, worauf sie sich beziehen steht ggf. als Argument in den Klammern hinter ihnen:�hj<  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*J� h+h,hj+  hhubhp)��}�(hhh]�hu)��}�(hhh]�hz)��}�(h�liste = [1, 2, 3]�h]�h�liste = [1, 2, 3]�����}�(hhhjQ  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhjN  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J� h+h,hjK  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj+  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(liste)�h]�h�print(liste)�����}�(hhhjs  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhjp  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J� h+h,hjm  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��
[1, 2, 3]
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hjm  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj+  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(len(liste))�h]�h�print(len(liste))�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J	 h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��3
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J	 h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj+  hhh+h,h*K ubeh}�(h]��id1�ah!]�h#]�h%]�j  ah']�uh)h
h*J� h+h,hj  hhj  Kubh)��}�(hhh]�(h)��}�(h�Methoden�h]�h�Methoden�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*J"0 h+h,hj�  ubh?)��}�(hX*  Daneben kennen wir aber auch schon Befehle, die mit einem Punkt an Objekte angehängt werden. Eine Liste ist ein solches **Objekt**. Jedes Objekt hat Methoden, auf die wir zurückgreifen können. Diese Methoden können wir aber nicht auf ein Objekt eines anderen Typs anwenden (meistens zumindest).�h]�(h�yDaneben kennen wir aber auch schon Befehle, die mit einem Punkt an Objekte angehängt werden. Eine Liste ist ein solches �����}�(h�yDaneben kennen wir aber auch schon Befehle, die mit einem Punkt an Objekte angehängt werden. Eine Liste ist ein solches �hj�  hhh+Nh*Nubh�)��}�(h�Objekt�h]�h�Objekt�����}�(h�Objekt�hj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*J#0 h+h,hj�  hhubh��. Jedes Objekt hat Methoden, auf die wir zurückgreifen können. Diese Methoden können wir aber nicht auf ein Objekt eines anderen Typs anwenden (meistens zumindest).�����}�(h��. Jedes Objekt hat Methoden, auf die wir zurückgreifen können. Diese Methoden können wir aber nicht auf ein Objekt eines anderen Typs anwenden (meistens zumindest).�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h>h*J#0 h+h,hj�  hhubh?)��}�(h�mSchauen wir uns einige nützliche Methoden des Listen-Objektes an :-) (du brauchst sie dir nicht alle merken)�h]�h�mSchauen wir uns einige nützliche Methoden des Listen-Objektes an :-) (du brauchst sie dir nicht alle merken)�����}�(h�mSchauen wir uns einige nützliche Methoden des Listen-Objektes an :-) (du brauchst sie dir nicht alle merken)�hj"  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h>h*J%0 h+h,hj�  hhubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�5# ein Element anhängen
liste.append(4)

print(liste)�h]�h�5# ein Element anhängen
liste.append(4)

print(liste)�����}�(hhhj7  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj4  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J2W h+h,hj1  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��[1, 2, 3, 4]
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J2W h+h,hjK  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj1  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�># ein Element an einem bestimmten Index entfernen
liste.pop(2)�h]�h�># ein Element an einem bestimmten Index entfernen
liste.pop(2)�����}�(hhhjt  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhjq  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*JB~ h+h,hjn  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(�data�h�)���
text/plain��2�s}�h��sb�execution_count�K'�metadata�h�)��}�h��sbh��execute_result�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*JB~ h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hjn  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�hu)��}�(hhh]�hz)��}�(h�b# wir sehen, dass die Methode nicht die aktualisierte Liste, sondern das entfernte Element liefert�h]�h�b# wir sehen, dass die Methode nicht die aktualisierte Liste, sondern das entfernte Element liefert�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*JR� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(liste)�h]�h�print(liste)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*Jb� h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��[1, 4, 3, 4]
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*Jb� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�hu)��}�(hhh]�hz)��}�(hXK   # Ein Element an einer bestimmten Stelle einfügen
# das erste Argument bei insert gibt an, welches Element in die Liste eingefügt wird, 
# das zweite Argument bei insert gibt an, an welcher Stelle das Element eingefügt wird; 
# beachte, dass der Index des ersten Elements in einer Liste 0 ist! 
liste.insert(1, 4)

print(liste)�h]�hXK   # Ein Element an einer bestimmten Stelle einfügen
# das erste Argument bei insert gibt an, welches Element in die Liste eingefügt wird, 
# das zweite Argument bei insert gibt an, an welcher Stelle das Element eingefügt wird; 
# beachte, dass der Index des ersten Elements in einer Liste 0 ist! 
liste.insert(1, 4)

print(liste)�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*Jr� h+h,hj  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�hu)��}�(hhh]�hz)��}�(h�5# ein Element entfernen
liste.remove(4)

print(liste)�h]�h�5# ein Element entfernen
liste.remove(4)

print(liste)�����}�(hhhj9  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj6  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J� h+h,hj3  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�`# den Index eines Elementes angeben (die erste Stelle, an der es vorkommt)
print(liste.index(3))�h]�h�`# den Index eines Elementes angeben (die erste Stelle, an der es vorkommt)
print(liste.index(3))�����}�(hhhj[  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhjX  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�A h+h,hjU  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��4
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�A h+h,hjo  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hjU  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(liste.index(4))�h]�h�print(liste.index(4))�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�h h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��1
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�h h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�print(liste.count(4))�h]�h�print(liste.count(4))�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J�� h+h,hj�  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��3
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J�� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubhp)��}�(hhh]�(hu)��}�(hhh]�hz)��}�(h�[# mit reverse können wir die Reihenfolge einer Liste umkehren
liste.reverse()
print(liste)�h]�h�[# mit reverse können wir die Reihenfolge einer Liste umkehren
liste.reverse()
print(liste)�����}�(hhhj	  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)hyhj	  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)hth*J¶ h+h,hj	  hhubh�)��}�(hhh]�h�)��}�(h�]�h�)��(h��stdout�h��stream�h��[1, 4, 4, 4, 3]
�u}�h��sbah�h�h�h�)��}�h��sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)h�h*J¶ h+h,hj&	  hhubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�hj	  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)hohj�  hhh+h,h*K ubeh}�(h]��methoden�ah!]�h#]��methoden�ah%]�h']�uh)h
h*J"0 h+h,hj  hhubeh}�(h]��funktionen-vs-methoden�ah!]�h#]��funktionen vs. methoden�ah%]�h']�uh)h
h*J�l h+h,hhhhubeh}�(h]�h!]�h#]�h%]�h']��source�h,uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j|	  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j  j  �
funktionen�Nj�  j�  j�  j�  j�  j�  j}  jz  j  j  j  j  jV	  jS	  jN	  jK	  u�	nametypes�}�(j  Nj�	  Nj�  Nj�  Nj�  Nj}  Nj  Nj  NjV	  NjN	  Nuh}�(j  hj  h-j�  hNj�  j�  j�  j�  jz  j�  j  j�  j  j  jS	  j  j�  j+  jK	  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�	  Ks��R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhh�fm_substitutions�}�ub.