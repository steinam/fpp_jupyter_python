## OpenCV: Helligkeit verändern

In dieser Lektion lernst du:

- Wie das Bild intern gespeichert wird
- Wie du die Helligkeit vom Bild verändern kannst

%matplotlib inline

import matplotlib.pyplot as plt
import numpy as np
import cv2

img = cv2.imread("bild.jpg")

print(img[0][0])

r = np.array([1, 2, 3, 100], dtype="uint8")
r = r + 250
print(r)

increased = img + 50

print(img[0][0])
print(increased[0][0])

i = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(i)
plt.show()

i = cv2.cvtColor(increased, cv2.COLOR_BGR2RGB)
plt.imshow(i)
plt.show()

