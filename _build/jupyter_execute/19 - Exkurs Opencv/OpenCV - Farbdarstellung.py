## OpenCV: Farbdarstellung

In dieser Lektion lernst du:

- Wie die Farben repräsentiert werden können
- Wie du die Grafik mit echten Farben anzeigen kannst
- Wann & Warum du Graustufen-Bilder verwenden solltest

%matplotlib inline

import matplotlib.pyplot as plt
import numpy as np
import cv2

img = cv2.imread("bild.jpg")

print(img[0][0])

i = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
print(i[0][0])
plt.imshow(i)
plt.show()

g = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

plt.imshow(g, "gray")
plt.show()

