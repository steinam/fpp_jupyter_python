## Gesichtserkennung mit OpenCV

In dieser Lektion lernst du:

- Wie du OpenCV beauftragen kannst, ein Gesicht in einem Bild zu finden

%matplotlib inline

import matplotlib.pyplot as plt
import numpy as np
import cv2

img = cv2.imread("bild.jpg")

i = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(i)
plt.show()

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
plt.imshow(gray, "gray")
plt.show()

classifier = cv2.CascadeClassifier("../data/haarcascades/haarcascade_frontalface_alt2.xml")

faces = classifier.detectMultiScale(gray, minNeighbors=10)

print(faces)

c = img.copy()
for face in faces:
    x, y, w, h = face
    cv2.rectangle(c, (x, y), (x + w, y + h), (0, 255, 0), 10)
    print(face)

i = cv2.cvtColor(c, cv2.COLOR_BGR2RGB)
plt.imshow(i)
plt.show()

