## OpenCV: Helligkeit verändern

In dieser Lektion lernst du:

- Wie das Bild intern gespeichert wird
- Wie du die Helligkeit vom Bild verändern kannst

%matplotlib inline

import matplotlib.pyplot as plt
import numpy as np
import cv2

img = cv2.imread("bild.jpg")

print(img.shape)

z = np.zeros((1000, 1500, 3), dtype="uint8") + 50

increased = cv2.add(img, z)

print(img[0][0])
print(z[0][0])
print(increased[0][0])

i = cv2.cvtColor(increased, cv2.COLOR_BGR2RGB)
plt.imshow(i)
plt.show()

i = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(i)
plt.show()

