## Opencv: Bilder mit Python bearbeiten

OpenCV stellt uns für Python diverse Funktionalitäten bereit, mit denen wir mit Bildern arbeiten können. Schauen wir uns das mal genauer an...

%matplotlib inline

import matplotlib.pyplot as plt
import numpy as np
import cv2

img = cv2.imread("bild.jpg")

img.shape

a = np.array([
    [1, 2, 3],
    [4, 5, 6]
])
print(a)
print(a.shape)

plt.imshow(img)
plt.show()

