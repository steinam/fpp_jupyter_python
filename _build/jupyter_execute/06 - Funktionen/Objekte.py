### Funktionen
Bei ihrem Aufruf stehen Funktionen "für sich" und das, worauf sie sich beziehen steht ggf. als Argument in den Klammern hinter ihnen:

liste = [1, 2, 3]

print(liste)

print(len(liste))

### Methoden
Daneben kennen wir aber auch schon Befehle, die mit einem Punkt an Objekte angehängt werden. Eine Liste ist ein solches **Objekt**. Jedes Objekt hat Methoden, auf die wir zurückgreifen können. Diese Methoden können wir aber nicht auf ein Objekt eines anderen Typs anwenden (meistens zumindest).

Schauen wir uns einige nützliche Methoden des Listen-Objektes an :-) (du brauchst sie dir nicht alle merken)

# ein Element anhängen
liste.append(4)

print(liste)

Genauso bieten auch andere "Objekte" in Python Methoden an. Beispielsweise ist der `.split()` - Aufruf auch ein Methodenaufruf:

"Hallo,Welt".split(",")





