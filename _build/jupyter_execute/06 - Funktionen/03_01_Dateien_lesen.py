# Eine txt-Datei lesen und ihren Inhalt ausgeben



# Wir öffnen die Datei lesen.txt zum Lesen ("r") und speichern ihren Inhalt in die Variable file
file = open("lesen.txt", "r")

# Wir gehen alle Zeilen nacheinander durch
# In der txt-Datei stehen für uns nicht sichtbare Zeilenumbruchszeichen, durch die jeweils das Ende einer Zeile markiert ist
for line in file:
    # Eine Zeile ohne Zeilenumbruch ausgeben
    print(line.strip())
    
file.close()

**Aufgabe**

Wandeln Sie das Programm in eine Funtion um. Der Name der zu lesenden Datei soll als Parameter übergeben werden könenn. Der Inhalt der Datei soll als String zurückgegeben werden. 

