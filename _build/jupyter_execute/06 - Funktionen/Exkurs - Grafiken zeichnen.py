## Exkurs: Grafiken zeichnen

In dieser Lektion lernst du, wie du schicke Grafiken zeichnen kannst.

### Schritt 1: Modul `matplotlib` einbinden + konfigurieren

%matplotlib inline
import matplotlib.pyplot as plt

### Schritt 2: Grafik zeichnen

xs = [1, 2, 5]
ys = [4, 7, 5]

plt.plot(xs, ys)
plt.show()

