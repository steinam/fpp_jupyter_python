## CSV lesen (und Daten überspringen)

In dieser Lektion lernst du:

- Wie du eine CSV-Datei einliest, und Zeilen überspringen kannst.

with open("datei.csv") as file:
    for line in file:
        data = line.strip().split(";")
        
        if int(data[1]) < 2000000:
            continue
        
        if data[2] == "BUD":
            continue
        
        print(data)
        
        #if data[2] == "BER" or data[2] == "BUD":
        #    print(data[2])
        #    print(data)

int("1800000") >= 2000000

