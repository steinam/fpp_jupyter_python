# 25 Eine csv-Datei öffnen
csv steht für comma separated values. Auch solche csv-Dateien können wir mit Python auslesen.

with open("datei.csv") as file:
    for line in file:
        data = line.strip().split(";")
        print(data[0] + ": " + data[1])