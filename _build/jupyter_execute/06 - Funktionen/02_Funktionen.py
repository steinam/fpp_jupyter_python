# Funktionen - Teil 2

## Funktionen mit mehreren Argumenten

Eine Funktion darf auch mehrere Argumente enthalten:

**def Funktionenname(Argument1, Argument2, ...):**
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **Code, in dem mit Argument1, Argument2,... gearbeitet wird**

def multi_print(name, count):
    for i in range(0, count):
        print(name)
        
multi_print("Hallo!", 5)

## Funktionen in Funktionen
Funktionen können auch ineinander geschachtelt werden:

def weitere_funktion():
    multi_print("Hallo!", 3)
    multi_print("Welt!", 3)

weitere_funktion()

## Einen Wert zurückgeben
Bislang führen wir mit Funktionen einen Codeblock aus, der von Argumenten abhängen kann. Funktionen können aber auch mittels des Befehls **return** Werte zurückgeben. 

def return_element(name):
    return name

print(return_element("Hi"))

Solche Funktionen mit return können wir dann wie Variablen behandeln:

def return_with_exclamation(name):
    return name + "!"

if return_with_exclamation("Hi") == "Hi!":
    print("Right!")
else:
    print("Wrong.")

def maximum(a, b):
    if a < b:
        return b
    else:
        return a

result = maximum(4, 5)
print(result)

## Zeit zu spielen
- Schreibe jetzt selbst eine Funktion mit mehreren Argumenten und return! :-)




