## Aufgabe!

Finde heraus, wie oft der Name "Max" als männlicher Vorname in Kalifornien zwischen 1950 und 2000 (jeweils einschließlich) vergeben wurde! Verwende dazu die bereitgestellte .csv - Datei (../data/names.csv)!

n = "1975"

print(int(n) < 1990)

years = ["Year", "1990", "1992"]

for year in years:
    if year == "Year":
        continue
    print(int(year))

