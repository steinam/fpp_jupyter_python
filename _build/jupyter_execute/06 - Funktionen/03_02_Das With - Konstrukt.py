# Dateien öffnen mit with
Wenn wir Dateien mit einer with-Konstruktion öffnen, dann brauchen wir sie nicht mehr explizit mit der close()-Methode schließen.

with open("lesen.txt", "r") as file:
    for line in file:
        print(line)

Ändern Sie die Funktion aus 03_01 entsprechend ab.

