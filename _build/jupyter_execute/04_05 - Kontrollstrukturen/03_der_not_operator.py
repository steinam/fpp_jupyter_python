## Der `not` - Operator

Der `not` - Operator dreht das boolesche Ergebnis einer Bedingung um.

Ob die Formulierung solcher Bedingungen sinnvoll ist, ist durchaus diskutierbar.

age = 25

if not age >= 30:
    print("ausgeführt")
    
if age < 30:
    print("ausgeführt")

Je nach Anwendungsfall kann ein `not` einen `else`-Zweig überflüssig machen.

names = ["Max", "Nadine"]

if "Moritz" not in names:
    print("Moritz ist nicht in der Liste enthalten")
    
if not "Moritz" in names:
    print("Moritz ist nicht in der Liste enthalten")

