# Kontrollstrukuren in Python

## if

Bislang haben wir nur Code gesehen, der von oben nach unten ausgeführt wird. In einer **If-Else-Struktur** werden bestimmte Abschnitte des Codes nur dann ausgeführt, wenn auch bestimmte Bedingungen erfüllt sind. Damit können wir Entscheidungen programmieren!

Schauen wir uns ein Beispiel an:

n = 30

if n < 42:
    print("Die Zahl n ist kleiner als 42")

print("Ich bin nicht mehr eingerückt!")

Es ist wichtig, dass der Code unterhalb der Bedingung eingerückt ist! 

## else
Diese if-Struktur lässt sich um ein **else** erweitern, sodass auch in dem Fall, dass die Bedingung nicht zutrifft, ein bestimmter Code ausgeführt wird. Falls die if-Bedingung erfüllt ist, wird natürlich der else-Block nicht mehr ausgeführt.

m = 10

if m < 5:
    print("m ist kleiner als 5")
else:
    print("ist nicht der Fall")

## Spiel doch jetzt ein wenig mit dem herum, was du gelernt hast:
- Schreibe deine eigene if-else-Bedingung! :-)



