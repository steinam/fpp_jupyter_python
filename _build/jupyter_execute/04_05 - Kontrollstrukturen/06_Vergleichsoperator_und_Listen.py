# Vergleichsoperatoren und Listen

Listen können mit Hilfe des **in**-Operators sehr schnell auf das Vorhandensein eines Wertes überprüft werden.

Fügen Sie folgenden Code in die untenstehe Zeile ein und führen Sie diese aus

```python
students = ["Max", "Monika", "Erik", "Franziska"]

print("Monika" in students)
print("Moritz" in students)
```




students = ["Max", "Monika", "Erik", "Franziska"]
print("Moritz" in students)

```python

if "Monika" in students:
    print("Ja, die Monika studiert hier!")
else:
    print("Nein, die Monika studiert hier nicht!")
    
if "Moritz" in students:
    print("Ja, die Moritz studiert hier!")
else:
    print("Nein, der Moritz studiert hier nicht!")
```

## Übrigens: Das klappt auch auf Strings!

sentence = "Ja, die Monika studiert hier!"

if "!" in sentence:
    print("JA")
else:
    print("NEIN")

