# elif - noch mehr Entscheidungen
Wir können in einer if-else-Struktur nur zwei Fälle abfragen, nämlich, ob eine Bedingung wahr ist (True) oder nicht (False). Wenn wir ein Entscheidungsmodell programmieren wollen, in dem noch mehr Bedingungen gecheckt werden sollen, müssen wir bislang mehrere if-else-Strukturen ineinander verschachteln. 

Folgendes Beispiel fragt beispielsweise die Währungseinheit ab. Die notwendigen verschachtelten else - if - Kombinationen machen das Beispiel schwer lesbar.

currency = "€"

if currency == "$":
    print("US-Dollar")
else:
    if currency == "¥":
        print("Japanischer Yen")
    else:
        if currency == "€":
            print("Euro")
        else:
            if currency == "฿":
                print("Thai Baht")

Durch die Erweiterung um **elif** können wir innerhalb einer if-else-Struktur beliebig viele Bedingungen checken. 

elif ist die Kurzfassung von else if: Eine elif-Option deckt also den Fall ab, dass die if-Bedingung nicht erfüllt (False) ist, aber eine weitere Bedingung True ist.

currency = "HKD"

if currency == "$":
    print("US-Dollar")
elif currency == "¥":
    print("Japanischer Yen")
elif currency == "€":
    print("Euro")
elif currency == "฿":
    print("Thai Baht")
else:
    print("Sonst")

## Platz für eigene Experimente:
- Baue selbst eine if-elif-else-Struktur! :-)




