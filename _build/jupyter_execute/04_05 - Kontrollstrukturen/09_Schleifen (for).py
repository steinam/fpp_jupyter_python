# Die for-Schleife
Neben der while-Schleife, die wir schon kennen gelernt haben, gibt es noch die **for-Schleife**. Hier durchläuft eine Schleifenvariable nacheinander die Werte in einer ebenfalls anzugebenden Sequenz.

Bei dieser Sequenz kann es sich z. B. um eine Liste handeln.

liste = [5, 8, 11]
for i in liste:
    print(i)

liste = ["Max", "Moritz", "Monika"]
for i in liste:
    print(i)

Wir sehen, dass unsere Schleifenvariable i nacheinander und automatisch die Werte aus der Liste annimmt. 

### Das range-Objekt
Als Sequenz für eine for-Schleife braucht man nicht zwangsläufig eine Liste. Häufig greift man stattdessen auf ein **range**-Objekt zurück:

print(range(0,10))

for i in range(0, 10):
    print(i)

# Hier summieren wir alle Zahlen von 1 bis 10 mithilfe einer for-Schleife und eines range-Objektes auf
sum = 0
for i in range(1, 11):
    sum += i
print(sum)

### Spiel doch jetzt ein wenig mit dem, was du gelernt hast, herum:
- Schreibe eine for-Schleife unter Verwendung eines range-Objektes! :-)






