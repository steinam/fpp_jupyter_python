# Die while-Schleife
Ein Code-Block innerhalb einer if-elif-else-Struktur wird jeweils nur einmal ausgeführt. Bei Schleifen wie der **while-Schleife** wird ein Code-Block so lange mehrmals hintereinander ausgeführt, bis eine Abbruchbedingung erfüllt ist.

counter = 0

while counter < 10:
    print(counter)
    counter = counter + 1
    
print("Hallo Welt")

Innerhalb einer Schleife **muss** sich **unbedingt** ein Zustand in jedem Schritt verändern, damit die Schleifenbedingung nicht dauerhaft erfüllt ist, und das Programm die Schleife auch wieder verlassen kann:

students = ["Moritz", "Klara", "Monika", "Max"]
i = 0
while i < len(students):
    print(students[i])
    i = i + 1

### Spiel doch jetzt ein wenig mit dem, was du gelernt hast, herum:
- Baue selbst eine while-Schleife und achte darauf, dass die Bedingung irgendwann nicht mehr erfüllt ist, indem du in dem eingerückten Codeblock einen passenden Zustand veränderst!  :-)



