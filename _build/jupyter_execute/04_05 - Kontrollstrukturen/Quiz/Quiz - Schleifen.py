## Quiz: Schleifen

#### Aufgabe 1

Wie prüfst du, ob das Element `"Treffer"` in folgender Liste enthalten ist?

```python
liste = ["Fail", "Fail", "Treffer", "Fail", "Volltreffer"]
```

- A) `"Treffer" in liste`
- B) `"Volltreffer" in liste`
- C) `liste.included("Treffer")`
- D) `liste in "Treffer"`

Richtige Lösung: 

#### Aufgabe 2

2.) Wie lautet in Python die Kurzschreibweise von else if?

- A) `elseif`
- B) `elsif`
- C) `elif`
- D) `elsf`

Richtige Lösung: 

#### Aufgabe 3

Wie häufig wird folgende `for` - Schleife durchlaufen?

```python
for i in range(1, 10):
    print(i)
```

- A) 10 mal
- B) 11 mal
- C) 8 mal
- D) 9 mal

Richtige Lösung: 

#### Aufgabe 4

Worauf musst du beim Verwenden einer `while` - Schleife auf jeden Fall achten?

- A) die Schleifenbedingung in Klammern zu setzen
- B) dass die Schleife terminiert, sich also keine Endlosschleife bildet
- C) die Schleife durch das Signalwort `stop` zu beenden, insbesondere muss hinter `stop` auch immer ein Semikolon stehen
- D) den Code-Block in geschweifte Klammern zu setzen

Richtige Lösung: 