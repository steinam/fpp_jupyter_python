## Quiz: If-Abfragen & Booleans

#### Aufgabe 1: Wird "Hallo Welt" ausgegeben?

Quellcode:
```python
if True and False:
    print("Hallo Welt")
```

- A: Ja
- B: Nein

Richtige Antwort: 

#### Aufgabe 2: Wird "Hallo Welt" ausgegeben?

Quellcode:
```python
if True or False:
    print("Hallo Welt")
```

- A: Ja
- B: Nein 

Richtige Antwort: 

#### Aufgabe 3

Um Verträge zu unterschreiben, muss eine Person i. d. R. volljährig (18 Jahre alt) sein. Wie würdest du diese Bedingung mit Python formulieren? 

Hinweis: Pass hier auf den Unterschied zwischen `<` und `<=` bzw. `>` und `>=` auf! Überprüfe dazu den Randfall (Person ist 18 Jahre) zusätzlich manuell im Kopf!

- A: `age > 18`
- B: `age < 18`
- C: `age >= 18`
- D: `age <= 18`

Richtige Antwort: 