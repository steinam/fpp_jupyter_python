# Vergleichsoperatoren

Python kennt alle klassischen mathematischen Vergleichsoperatoren

if 6 < 5:
    print("JA")

print(6 < 5)
print(5 < 6)

b = False
print(b)

result = 5 < 6
if result:
    print("5 ist kleiner als 6")

print(5 < 6)

print(5 > 6)
print(6 > 5)

print(5 == 5)
print(5 == 4)

word = "Hallo"
print(word == "Hallo")
print(word == "Welt")

word = "Hallo"
print(word != "Hallo")
print(word != "Welt")

print(5 < 5)
print(5 <= 5)

print(5 > 5)
print(5 >= 5)

Vergleichsoperatoren können sich auf Wertgleichheit, aber auch auf Objektgleichheit beziehen. In zweiten Fall wird intern geprüft, ob die gleiche ObjektID vorliegt. Das untere Beispiel zeigt damit auch, **dass nicht für jede Variable ein Wert gespeichert wird (c), wenn dieser bereits vorher einer anderen Variable zugewiesen wurde (a)**.

a = "Hall"
b = "hall"
c = "Hall"


print(a==b)
print(a is c)

print(id(a))
print(id(c))







