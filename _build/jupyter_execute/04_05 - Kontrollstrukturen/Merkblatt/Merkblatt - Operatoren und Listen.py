# Operatoren und Listen
Wir können mit **in** checken, ob ein Element in einem anderen Element enthalten ist.

### Der in-Operator und Listen

Operatoren gibt es auch in Bezug auf Listen; wir können etwa mit dem **in**-Operator prüfen, ob ein Element in einer Liste enthalten ist. 
 

Formal sieht die Syntax so aus: **Element <span style="color:green">in</span> Liste**


students = ["Max", "Monika", "Erik", "Franziska"]

print("Monika" in students)
print("Moritz" in students)

Das Resultat einer solchen Abfrage ist ein Bool, d. h., dass der Wert entweder True oder False ist. Somit können wir Ausdrücke mit dem in-Operator auch in if-else-Strukturen verwenden:

if "Monika" in students:
    print("Ja, die Monika studiert hier!")
else:
    print("Nein, die Monika studiert hier nicht!")
    
if "Moritz" in students:
    print("Ja, der Moritz studiert hier!")
else:
    print("Nein, der Moritz studiert hier nicht!")

### Der in-Operator und Strings
Tatsächlich lässt sich der in-Operator auch auf Strings anwenden. Wir können also z. B. checken, ob ein Buchstabe bzw. ein Zeichen in einem Wort enthalten ist, oder ein Wort in einem Satz, usw.

sentence = "Ja, die Monika studiert hier!"

if "!" in sentence:
    print("JA")
else:
    print("NEIN")

word = "Studium"

if "udi" in word:
    print("JA")
else:
    print("NEIN")

### Spiel doch jetzt ein wenig mit dem, was du gelernt hast, herum:
- Checke ein paar Strings mit dem in-Operator! :-)




