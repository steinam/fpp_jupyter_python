# Merkblatt: Schleifen

Schleifen erlauben es dir, dafür zu sorgen, dass bestimmte Teile vom Programm mehrfach ausgeführt werden. Das ist praktisch, wenn du z.B. eine Liste von Studierenden hast und dafür sorgen möchtest, dass z. B. ein print() - Befehl einmal für jeden Studierenden ausgeführt wird. 

Eine Schleife erlaubt dir, sowas in recht wenig Code auszudrücken!

## Die for-Schleife
Neben der while-Schleife, die wir schon kennen gelernt haben, gibt es noch die **for-Schleife**. Hier durchläuft eine Schleifenvariable nacheinander die Werte in einer ebenfalls anzugebenden Sequenz.

Bei dieser Sequenz kann es sich z. B. um eine Liste handeln:

liste = [5, 8, 10]
for i in liste:
    print(i)

liste = ["Max", "Moritz", "Monika"]
for i in liste:
    print(i)

Wir sehen, dass unsere Schleifenvariable i nacheinander und automatisch die Werte aus der Liste annimmt. 

### Das range-Objekt
Als Sequenz für eine for-Schleife braucht man nicht zwangsläufig eine Liste. Häufig greift man stattdessen auf ein **range**-Objekt zurück:

print(range(0,10))

for i in range(0, 10):
    print(i)

# Hier summieren wir alle Zahlen von 1 bis 10 mithilfe einer for-Schleife und eines range-Objektes auf
sum = 0
for i in range(1, 11):
    sum += i
print(sum)

## Die while-Schleife
Ein Code-Block innerhalb einer if-elif-else-Struktur wird jeweils nur einmal ausgeführt. Bei Schleifen wie der **while-Schleife** wird ein Code-Block so lange mehrmals hintereinander ausgeführt, bis eine Abbruchbedingung erfüllt ist:

counter = 0

while counter < 10:
    print(counter)
    counter = counter + 1
    
print("Hallo Welt")

Innerhalb einer Schleife **muss** sich **unbedingt** ein Zustand in jedem Schritt verändern, damit die Schleifenbedingung nicht dauerhaft erfüllt ist, und das Programm die Schleife auch wieder verlassen kann.

students = ["Moritz", "Klara", "Monika", "Max"]
i = 0
while i < len(students):
    print(students[i])
    i = i + 1

# Continue & Break

Wir können während eines Schleifendurchlaufs den aktuellen Durchlauf vorzeitig abbrechen und unmittelbar mit dem nächsten Schleifendurchlauf fortfahren (**continue**) oder auch die gesamte Schleife abbrechen (**break**).


### Continue
Wir brauchen einfach das Wort **continue** in eine Schleife zu schreiben, wenn an einer bestimmten Stelle zum neuen Schleifendurchlauf gesprungen werden soll:

for i in range(0, 10):
    if i == 3:
        continue
    print(i)

In dem obigen Beispiel wird für den Wert 3 der print()-Befehl übersprungen.

for i in range(1, 10):
    print(i)

### Break
Auch **break** schreiben wir einfach in eine Zeile, und schon wird die ganze Schleife abgebrochen, wenn das Programm diese Stelle erreicht:


for i in range(0, 10):
    if i == 3:
        break
    print(i)

liste = [4, 6, 7, 2, 4, 6, 7]

s = 0

for element in liste:
    s = s + element
    if s > 10:
        break
    
print(s)