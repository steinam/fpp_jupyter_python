### Vergleichsoperatoren

Um die Bedingungen in den if-else-Strukturen besser zu verstehen, schauen wir uns **Vergleichsoperationen** an. Das wird uns helfen, eine Vielfalt an Bedingungen formulieren zu können.

### Ungleichheitsoperatoren und Bools

if 6 < 5:
    print("JA")

Warum kommt es zu keiner Ausgabe mit print()? Schauen wir uns doch Ausdrücke mit Ungleichheitszeichen (<,>) im Detail an:

print(6 < 5)
print(5 < 6)

**True** und **False** (Großschreibung beachten!) sind weitere _feststehende Ausdrücke_ in Python. Neben Strings (Zeichenketten), Ganzzahlen (Integer) und Fließkommazahlen (Floats) bilden sie einen weiteren Datentyp - den **Bool**.

b = False
print(b)

Genauer wird eine if-Bedingung also nur dann ausgeführt, wenn nach dem if ein Bool mit dem Wert True steht: 

result = 5 < 6
if result:
    print("5 ist kleiner als 6")

print(5 < 6)

### Der Gleichheitsoperator

Neben Ungleichheiten können wir natürlich auch Gleichheiten abfragen, und zwar mit **==**

print(5 == 5)
print(5 == 4)

if 5 == 5:
    print("5 ist 5")

Mittels des Gleichheitsoperators können wir auch die Zustände _größer gleich_ (**>=**) und _kleiner gleich_ (**<=**) abfragen:

print(5 < 5)
print(5 <= 5)
print(5 >= 5)

### Strings vergleichen 

Wir können nicht nur Zahlen miteinander vergleichen, sondern auch Strings:

word = "Hallo"
print(word == "Hallo")
print(word == "Welt")

### Der Ungleichheitsoperator

Auf Ungleichheit checken wir mit dem Zeichen **!=**

word = "Hallo"
print(word != "Hallo")
print(word != "Welt")

Auch Zahlen kann man auf Ungleichheit hin miteinander vergleichen:

zahl = 4
print(zahl != 4)
print(zahl != 5.5)

### Spiel doch jetzt ein wenig mit dem, was du gelernt hast, herum:
- Formuliere eigene Vergleichsoperationen mit den Operatoren aus dieser Lektion! :-)




