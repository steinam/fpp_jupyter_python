## Übung: Hundejahre

Üblicherweise entspricht ein Menschenjahr ungefähr 7 Hundejahren. Diese einfache Annahme lässt aber unberücksichtigt, dass Hunde in ca. 2 Jahren erwachsen sind. Als Konsequenz nehmen manche Menschen an, dass man die ersten beiden Menschenjahre mit 10.5 Hundejahren verrechnen sollte und dann jedes weitere Menschenjahr mit 4 Hundejahren.

- Schreibe ein Programm, dass die oben beschriebene Umrechnug vornimmt.
- Stellen Sie sicher, dass das Programm für negative Zahlen eine Fehlermeldung ausgibt.
- Gebe ebenso den Wert der alten Berechnung aus (1 Menschenjahr = 7 Hundejahre)



#your code here
a = int(input("Zahl eingeben"))
print(a)
if  a > 10:
    # print(a + " ist größer als 10")
    #print(f'{a}' {" ist größer als 10"}  )
    print("{0} ist größer als 10".format(a))
    print( f'{a} {"is bigger than 10"}' )

## Geräuschpegel

<!--
The following table lists the sound level in decibels for several common noises.
-->

Die folgende Tabelle zeigt die Lautstärle in Dezibel für ausgewählte Geräte

| Gerät           |  Dezibel |
| :---------------| ----------:|
| Presslufthammer |  130       |
| Benzinrasenmäher|  106       |
| Wecker          |   70       |
| Ruheraum        |   40       |

<!--
Write a program that reads a sound level in decibels from the user. If the user
enters a decibel level that matches one of the noises in the table then your program
should display a message containing only that noise. If the user enters a number
of decibels between the noises listed then your program should display a message
indicating which noises the level is between. Ensure that your program also generates
reasonable output for a value smaller than the quietest noise in the table, and for a
value larger than the loudest noise in the table.
-->
Schreibe ein Programm, dass den Geräuschpegel vom User abfragt. Wenn der Benutzer einen exakten Wert angibt, soll genau diees Gerät angegeben werden. Wenn der Wert dazwischen liegt, soll das Programm die benachbarten Geräusche anzeigen.Stellen Sie sicher, dass das Programm auch aussagekräftige Ausgaben für Lautstärken macht, die größer oder kleiner als die angegebenen Werte sind.

#Hier Code eingeben
a = int(input("Zahl eingeben"))
if  a > 10:
    print("a ist größer als 10")


#Hier Code eingeben
a = int(input("Zahl eingeben"))
if  a > 10:
    print("a ist größer als 10")

