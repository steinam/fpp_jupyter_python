## Booleans (`and` und `or`)

Bedingungen gebel letzlich immer einen Wahrheitswert zurück (True/False). Man nennt diese Wert auh bool'sche Werte. Es können mehrere Bedingunen durch **and** bzw **or** kombiniert werden.

Fraglich ist letztendlich, wie diese Kombinationen nun insgesamt ausgewertet werden.

- bei **and** müssen alle Teilbedingunen jeweils WAHR sein, damit der Gesamtausdruck WAHR wird
- bei **oder** reicht eine Teilbedingung mit WAHR, damit der Gesamtausdruck WAHR ist



```python
age = 35

if age >= 30 and age <= 39:
    print("Diese Person ist in ihren 30-ern")
```


age = 35

if age >= 30 and age <= 39:
    print("Diese Person ist in ihren 30-ern")

age = 45
if age < 30 or age >= 40:
    print("Diese Person ist nicht in ihren 30-ern")

age = 25
print(age < 30)

above_30 = age >= 30

print(above_30)

age = 25

above_20 = age >= 20
print(above_20)

if age >= 20:
    print("if-Abfrage wurde ausgeführt")

print(True and True)
print(True and False)
print(False and True)
print(False and False)

print(True or True)
print(True or False)
print(False or True)
print(False or False)

Ein besonderes Augenmerk ist auch dann auf die Auswertung zu richten, wenn es mehr als zwei Verknüpfungen 
hintereinander gibt bzw. wenn die Verknüpfungen durch Klammern umschlossen sind.

AND und OR haben bei der Auswertung eine unterschiedliche Gewichtung. Eine AND-Verknüpfung ist höherwertiger als eine OR-Verknüpfung.

country = input("Land eingeben")

#country = "US"
age = 20

if (country == "US" and age >= 21) or (country != "US" and age >= 18):
    print("Diese Person darf Alkohol trinken")

