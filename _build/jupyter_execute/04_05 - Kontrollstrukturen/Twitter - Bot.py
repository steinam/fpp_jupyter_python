# Trump - Twitter - Bot
In dieser Lektion bauen wir einen Text-Generator, der zufällig generierte Tweets ausgibt :-)

## Exkurs: Zufallszahlen
Wir können in Python mit der **randint()**-Funktion zufällige Zahlen generieren. Doch um auf diese Funktion zurückzugreifen, müssen wir erst das random-Modul einbinden. :-)

import random

randint liefert zufällig Ganzzahlen aus einem Intervall, das wir der Funktion mitteilen müssen. Die beiden Grenzen und alle ganzen Zahlen innerhalb des Intervalls können dabei Werte von randint sein.

print(random.randint(0, 4))

print(random.randint(0, 4))

## Listen mit den Begriffen für unsere Tweets anlegen

Hier haben wir einfach mal ein paar häufig auftretende Begriffe aus den Tweets kopiert ;-)

part1 = ["Putin,", "Hillary,", "Obama", "Fake News,", "Mexico,", "Arnold Schwarzenegger", "The Democrats"]
part2 = ["no talent,", "on the way down,", "really poor numbers,", "nasty tone,", "looking like a fool,", "bad hombre,"]
part3 = ["got destroyed by my ratings.", "rigged the election.", "had a much smaller crowd.", "will pay for the wall."]
part4 = ["So sad", "Apologize", "So true", "Media won't report", "Big trouble", "Fantastic job", "Stay tuned"]

# Wir können auch Listen von Listen erstellen!
best_words = [part1, part2, part3, part4]
print(best_words)

## Den Tweet-Generator bauen

# your Code here







## Jetzt kannst du versuchen, den Tweet Generator zu erweitern. :-)



