class Hacker:
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname
        
    def get_name(self):
        print(self.firstname + " " + self.lastname)

hacker1 = Hacker("Max", "Müller")
hacker1.get_name()

a = Hacker("Erika", "Mustermann")
a.get_name()

