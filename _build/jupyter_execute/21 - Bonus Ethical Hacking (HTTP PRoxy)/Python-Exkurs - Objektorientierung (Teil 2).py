class Hacker:
    def get_name(self):
        print(self.firstname + " " + self.lastname)

hacker1 = Hacker()
hacker1.firstname = "Max"
hacker1.lastname = "Müller"

hacker1.get_name()

class Company:
    def get_name(self):
        print(self.name)
        
c = Company()
c.name = "Hacking GmbH"
c.get_name()

participants = [
    hacker1,
    c
]

for participant in participants:
    participant.get_name()

