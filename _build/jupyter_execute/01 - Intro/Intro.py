# Wie nutzt man Jupyter Notebook

Jupyter Notebook ist eine sehr komfortable Möglichkeit, Programmcode und erläuternden Text in einer einzigen Datei zu kombinieren. 

Texte, Bilder, Links, etc. werden mit Hilfe der Textauszeichnungssprache **Markdown** geschrieben, der Programmcode steht jeweils in sog. **Code**-Zeilen, die unabhängig voneinander ausgeführt und selbstverständlich jederzeit geändert werden können. Beides ist einer Datei mit der Endung **.ipynb** gespeichert.

Jupyter Notebooks funktionieren mit vielen Programmiersprachen (Kerneln), darunter eben auch Python.  

Um Notebooks nutzen zu können, muss 

- Python installiert sein
- Der Paketmanager pip installiert sein
- Über den Paketmanager bestimmte Zusatzfunktionalität installiert sein

Der folgende Text beschreibt die grundlgende Vorgehensweise

- Installer von Python der neuestens Version für sein Betriebssystem unter www.python.org downloaden
- Installer ausführen. 
  - Im Installer sollte der Pfad der Pythoninstallation der PATH-Umgebung hinzugefügt werden
  - PIP sollte zusätzlich installiert werden
- Nach der Installation eine Kommandozeile öffnen
- In der Kommandozeile **pip install notebook** eingeben
  Es wird das Jupyther -Paket installiert
- Auf der Kommandozeile in den Ordner wechseln, der die entsprechenden *.ipynb* - Dateien enthält
- In diesem Ort Jupyter starten mit Hilfe des folgendes Befehls in der Kommandozeile: **jupyter notebook**
- Es sollte sich ein Browserfenster öffnen, in dem Sie die entsprechenden Dateien auswählen können

Die in den folgenden Ordnern dargestellten Erläuterungen sollen mehr oder weniger ein Selbstlernskript darstellen, das zum Ausprobieren von Python dienen soll. Jede Einheit hat eine Themation als Schwerpunkt, welches mit erläuternden Texten, Codebeispielen und Aufgaben gefüllt ist. Musterlösungen werden ebenfalls später 