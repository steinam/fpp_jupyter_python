# Typen von Variablen prüfen - die type() und isinstance() Funktionen

Wir benutzen für die Beispiele wieder die bekannten Student und WorkingStudent-Klassen:

class Student():
    def __init__(self, firstname, surname):
        self.firstname = firstname
        self.surname = surname

    def name(self):
        return self.firstname + " " + self.surname
        
class WorkingStudent(Student):
    def __init__(self, firstname, surname, company):
        super().__init__(firstname, surname)
        self.company = company
        
    def name(self):
        return super().name() + " (" + self.company + ")"

w_student = WorkingStudent("Max", "Müller", "ABCDEF GmbH")
student = Student("Monika", "Mustermann")

### Den Typ überprüfen mit type()
Mit der **type()**-Funktion können wir den Typ eines Objektes feststellen: 

print(type(w_student))
print(type(student))

if type(w_student) == Student:
    print("Diese Zeile wird nur für einen Student ausgegeben")

if type(student) == Student:
    print("Hier hingegen steht ein richtiger Student")

### Checken, ob es sich um eine Instanz handelt mit isinstance()

Die Funktion **isinstance()** erhält zwei Parameter: die Variable und die Klasse bezüglich derer auf Zugehörigkeit der Variable geprüft werden soll. isinstance() liefert einen Bool zurück:

print(isinstance(w_student, WorkingStudent))
print(isinstance(w_student, Student))

print(isinstance(student, WorkingStudent))
print(isinstance(student, Student))

Da Student die Mutterklasse von  WorkingStudent ist, ist w_student auch bezüglich Student eine Instanz.

Nützlich wird die Funktion wenn wir nach Klassen filtern wollen, z. B. nur Instanzen von WorkingStudent ausgeben:

students = [
    WorkingStudent("Max", "Müller", "ABCDEF GmbH"),
    Student("Monika", "Mustermann"),
    Student("Erik", "Müller"),
    WorkingStudent("Franziska", "Mustermann", "XYZXYZ GmbH")
]

for student in students:
    ## alternativ: 
    ## if isinstance(student, WorkingStudent):
    if type(student) == WorkingStudent:
        print(student.name())