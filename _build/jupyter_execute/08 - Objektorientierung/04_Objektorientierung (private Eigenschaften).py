## Objektorientierung: Private Eigenschaften und Methoden

In dieser Lektion lernst du:

- Was private Eigenschaften sind...
- Was private Methoden sind...
- ... und wo / wozu du diese einsetzen solltest

class Student():
    
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname
        self.__term = 1
                
    def increase_term(self):
        if self.__term >= 9:
            return
        self.__term = self.__term + 1
        
    def get_term(self):
        return self.__term
    
    def name(self):
        print(self.firstname + " " + self.lastname + " (Semester: " + str(self.__term) + ")")
        
        
erik = Student("Erik", "Mustermann")
erik.increase_term()
erik.name()


Da die Variable __term mit 2 Unterstrichen beginnt, ist diese Eigenschaft **privat**, d. h. wir können von außen nicht auf sie zugreifen:

print(erik.__term)

