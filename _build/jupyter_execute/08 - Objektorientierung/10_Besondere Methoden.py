## In Python gibt es ein paar besondere Methoden, die unsere Klasse implementieren kann...

Damit kannst du dafür sorgen, dass:

- deine Klasse direkt ausgegeben werden kann,
- du len(variable) berechnen kannst.

Die str-Funktion

class PhoneBook():
    def __init__(self):
        self.__entries = {}
        
    def add(self, name, phone_number):
        self.__entries[name] = phone_number
        
    def get(self, name):
        if name in self.__entries:
            return self.__entries[name]
        else:
            return None
    
    def __str__(self):
        return "PhoneBook(" + str(self.__entries) + ")"
    
book = PhoneBook()
book.add("Mustermann", "+4912345678")
book.add("Müller", "+49123456789")

print(book)

Die repr-Methode

class PhoneBook():
    def __init__(self):
        self.__entries = {}
        
    def add(self, name, phone_number):
        self.__entries[name] = phone_number
        
    def get(self, name):
        if name in self.__entries:
            return self.__entries[name]
        else:
            return None
    
    def __str__(self):
        return "PhoneBook(" + str(self.__entries) + ")"
    
    def __repr__(self):
        return self.__str__()

        
book = PhoneBook()
book.add("Mustermann", "+4912345678")
book.add("Müller", "+49123456789")

print(book)

Die len-Methode

class PhoneBook():
    def __init__(self):
        self.__entries = {}
        
    def add(self, name, phone_number):
        self.__entries[name] = phone_number
        
    def get(self, name):
        if name in self.__entries:
            return self.__entries[name]
        else:
            return None
    
    def __len__(self):
        return len(self.__entries)

        
book = PhoneBook()
book.add("Mustermann", "+4912345678")
book.add("Müller", "+49123456789")

print(len(book))