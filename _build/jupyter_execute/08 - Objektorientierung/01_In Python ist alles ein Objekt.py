## In Python ist alles ein Objekt

### Zahlen sind Objekte
Wenden wir doch die type()-Funktion mal auf Variablen an, in die wir Zahlen gespeichert haben:

a = 13

print(type(a))

a = 12.5

print(type(a))

Wir sehen also, dass es sich bei den Variabletypen int und float um Klassen handelt!

### Verschiedene Darstellungen von Rechenperationen

12.5 + 5

Intern wird bei Python in diesem Fall aber die **\__add__()**-Funktion ausgerufen. Eine Addition sieht in objektorientierter Schreibweise etwas so aus:

a = 12.5
a.__add__(5)

(12.5).__add__(5)

Weitere Rechenoperationen sehen in objektorientierter Schreibweise wie folgt aus:

(12.5).__sub__(7)

(12.5).__mul__(5)

(12.5).__truediv__(6.25)

### Strings sind Klassen

s = "Hallo Welt"
print(type(s))

Wie du es dir schon gedacht hast, handelt es sich auch bei str um eine Klasse.

len(s)

Intern führt hier Python entsprechend die **\__len__()**-Methode aus:

s.__len__()

Aufgrund dieser internen Struktur können wir Funktionen wie len selbst implementieren und somit steuern:

class A:
    def __len__(self):
        return 6

instance = A()
len(instance)

