# Vererbung

Vererbung ist ein fundamentales Konzept der Objektorientierung, mit dem du Daten aufteilen und besser modellieren kannst.

Die Klasse Student kennen wir schon:

class Student():
    def __init__(self, firstname, surname):
        self.firstname = firstname
        self.surname = surname

    def name(self):
        return self.firstname + " " + self.surname

student = Student("Monika", "Mustermann")
print(student.name())

Wir wollen eine weitere Klasse definieren, die Ähnlichkeiten zu einer bereits bestehenden Klasse aufweist:

class WorkingStudent():
    
    def __init__(self, firstname, surname, company):
        self.firstname = firstname
        self.surname = surname
        self.company = company
    
    def name(self):
        return self.firstname + " " + self.surname
        

student = WorkingStudent("Max", "Müller", "ABCDEF GmbH")
print(student.name())

### Eine Klasse mit Vererbung definieren

Wir können uns sparen, gleiche Instanzvariablen und Methoden ein weiteres Mal zu definieren - dank Vererbung. Dazu verweisen wird innerhalb einer Klassendefinition auf eine andere:

# Als Parameter übergeben wir die Klasse, von der unsere neue Eigenschaften und Methoden vererbt werden soll (Mutterklasse)
class WorkingStudent(Student):
    
    def __init__(self, firstname, surname, company):
        # Die alten Instanzvariablendefinitionen werden unten hinfällig
        # self.firstname = firstname
        # self.surname = surname
        
        # mit super() zeigen wir Python an, dass die init()-Methode der Mutterklasse angewendet werden soll
        super().__init__(firstname, surname)
        self.company = company
    
    def name(self):
        # wieder verweisen wir mit super() auf die Methode der Mutterklasse, die wir für die Klasse WorkingStudent überschreiben
        return super().name() + " (" + self.company + ")"

student = WorkingStudent("Max", "Müller", "ABCDEF GmbH")
print(student.name())

students = [
    WorkingStudent("Max", "Müller", "ABCDEF GmbH"),
    Student("Monika", "Mustermann"),
    Student("Erik", "Müller"),
    WorkingStudent("Franziska", "Mustermann", "XYZXYZ GmbH")
]

for student in students:
    print(student.name())

Hier sehen wir, dass die verschiedenen name()-Methoden verschiedene Ausgaben liefern, obwohl wir mit demselben Namen auf sie zugreifen.