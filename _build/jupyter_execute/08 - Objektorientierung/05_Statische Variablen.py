# Statische Variablen
Wir können Variablen auch statt an Instanzen an Klassen binden. Sie heißen statisch, weil sie nicht mehr an einer einzelnen Instanz hängen: 

class Car:
    # das self von früher fehlt hier
    price = "expensive"

c = Car
print(c.price)

Auf die Variable price haben wir auch über die Klasse car Zugriff:

print(Car.price)

Car.price = "cheap"

print(c.price)

Wir haben also die Eigenschaft bei allen Instanzen geändert!

bobbycar = Car
print(bobbycar.price)

Wir haben also nachträglich die Eigenschaft price der Klasse geändert. Davon ist im Allgemeinen abzuraten!

Wie zuvor sollten wir daher Instanzvariablen definieren, die an eine Instanz gebunden sind und unabhängig von der Klasse verändert werden können:

class CarI:
    def __init__(self):
        self.price = "expensive"
        
c = CarI()
print(c.price)

