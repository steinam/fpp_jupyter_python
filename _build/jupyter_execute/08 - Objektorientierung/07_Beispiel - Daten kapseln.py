## Beispiel: Daten kapseln

In dieser Lektion lernst du ein Beispiel kennen, warum es sich lohnen kann, Daten mit Objektorientierung zu kapseln.

class PhoneBook():
    
    def __init__(self):
        self.__entries = {}
        
    # Nur mit der add-Methode können wir mit dem Dictionary kommunizieren    
    def add(self, name, phone_number):
        self.__entries[name] = phone_number
        
    def get(self, name):
        if name in self.__entries:
            return self.__entries[name]
        else:
            return None
        
book = PhoneBook()
book.add("Mustermann", "+4912345678")
book.add("Müller", "+49123456789")

print(book.get("Mustermann2"))

Vorteil: Übersichtlichkeit, Voraussetzung größere Anwendungen bauen (Modular)

