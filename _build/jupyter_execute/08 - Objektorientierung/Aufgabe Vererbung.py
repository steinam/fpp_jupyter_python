## Aufgabe: Vererbung

#### Aufgabe 1:

Vervollständige die Klasse "FileReader" so, dass bei Aufruf der lines() - Methode die Datei Zeile für Zeile eingelesen wird. Die lines() - Methode soll dann eine Liste der Zeilen in der Datei zurückgeben.

#### Aufgabe 2:

Erstelle die Klasse "CsvReader", sodass der "FileReader" erweitert wird. Bei Aufruf der lines() soll die Datei als .csv-Datei eingelesen werden, sprich es soll eine mehrdimensionale Liste zurückgegeben werden. 

**Wichtig:** Überlass' das Einlesen der Datei dem "FileReader", und erweitere die lines() - Methode im Csv-Reader um die Funktionalität, die benötigt wird, damit die mehrdimensionale Liste zurückgegeben wird!

class FileReader():
    pass

f = FileReader("./datei.csv")
print(f.lines())

# Hier soll ausgegeben werden:
# ["Nachname,Vorname", "Mustermann,Max", "Mueller,Monika"]

f = CsvReader("./datei.csv")
print(f.lines())

# Hier soll ausgegeben werden:
# [["Nachname", "Vorname"], ["Mustermann", "Max"], ["Mueller", "Monika"]]