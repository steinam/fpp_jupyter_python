# Objektorientierung: Constructor, Eigenschaften abändern

In dieser Lektion lernst du, wie du:

- mit Hilfe eines Constructors Eigenschaften einer Klasse definieren kannst
- Eigenschaften einer Instanz abänderst.

Wir haben die bereits die Klasse Students ein wenig erweitert:

class Student():
    
    # Das ist unser sogenannter Constructor: hier definieren wir die Variablen für die Klasse
    # self ist obligatorisch und bezieht sich immer auf das Objekt, das gerade angelegt wird
    # bei der Erzeugung der Instanz taucht self aber nicht als Parameter auf!
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname
    
    def name(self):
        print(self.firstname + " " + self.lastname)

erik = Student("Erik", "Mustermann")
erik.name()

Die Klassendefinition mit Constructor liefert also dieselben Ergebnisse wie die vorherige, umständlichere Definition.

Wir ergänzen eine weitere Methode:

class Student():
    
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname
        # Hier initialisieren wir die neue Variable term (Eigenschaft)
        self.term = 1
        
         # Mit dieser Methode erhöhen wir die Variable term um 1
    def increase_term(self):
        self.term = self.term + 1
    
        # name() gibt nunmehr zusätzlich die Anzahl der Semester aus
    def name(self):
        print(self.firstname + " " + self.lastname + " (Semester: " + str(self.term) + ")")
        

erik = Student("Erik", "Mustermann")
erik.name()

erik.increase_term()
erik.name()