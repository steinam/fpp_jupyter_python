# Strings

Wir können in Python nicht nur mit Zahlen, sondern auch mit Zeichenketten, sogenannten **Strings**, arbeiten. Darunter kannst du dir jegliche Abfolgen von Zeichen vorstellen, die in Anführungszeichen stehen. Zum Beispiel ist _"Hallo"_ ein String und _"Hallo Welt"_ ebenso, aber auch _"123.2"_ oder _"!Achtung!"_

### Allgemeines

Auch Strings können wir mit dem print()-Befehl ausgeben.

print("Hallo Welt")

Du kannst Strings wie Zahlen in Variablen speichern.

name = "Max"

print(name)

### Strings zusammenfügen

Du kannst zwei oder mehrere Zeichenketten auch mittels **+** zusammenfügen.

print("Ich bin: " + "Max")

print("Ich bin: " + name + ". Und wer bist du?")

Allerdings kommt es zu einer Fehlermeldung, wenn du versuchst, Zahlen und Strings zu addieren:

print("Ich bin: " + 4)

### Eine Zahl in einen String umwandeln
Du kannst diese Fehler korrigieren, indem du die Zahl in einen String umwandelst. Dazu hast du zwei Möglichkeiten:

1.) Du setzt Anführungszeichen um die Zahl und machst sie so zu einem String:

print("Ich bin: " + "4")

2.) Du wandelst die Zahl mit dem **str()**-Befehl in einen String um:

age = 22
print("Ich bin: " + "age")
print("Ich bin: " + str(age))

**Beachte, dass du mit "4" oder str(age) nicht mehr rechnen kannst!**

age = "22"

22 + int(age)

### Spiel doch jetzt ein wenig mit dem, was du gelernt hast, herum:
- Gib einige mittels + zusammengesetzte Strings per print() aus! :-)




