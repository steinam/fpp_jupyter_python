## Quiz: Listen

#### Aufgabe 1

Wie wird in Python eine Liste definiert?

- A) `{"Ivanka", "Donald"}`
- B) `["Ivanka", "Donald"]`
- C) `["Ivanka"; "Donald"]`
- D) `("Ivanka", "Donald")`

Richtige Antwort: 

#### Aufgabe 2

Wie greifst du auf das 3. Element zu?

```python
firstnames = ["Ivanka", "Donald", "Ivana", "Barron"]
```

- A) `firstnames[0]`
- B) `firstnames[1]`
- C) `firstnames[2]`
- D) `firstnames[3]`

Richtige Antwort: 

#### Aufgabe 3

Wie erweiterst du die Liste `wars` um ein weiteres, drittes Element?

```python
wars = [1, 2]
```

- A) `wars.add(3)`
- B) `wars[3] = 3`
- C) `wars.hinzufügen(3)`
- D) `wars.append(3)?`

Richtige Antwort: 