## Quiz: Kommentare und Zahlen

### Aufgabe 1

Mit welchem / welchen Zeichen beginnst du einen Kommentar in Python (der nicht als Code ausgeführt werden soll)?

- A) `#`
- B) `**`
- C) `%`
- D) `$`

Korrekte Lösung: 

### Aufgabe 2

Welche Zahlen werden bei der Ausführung von folgendem Programm ausgegeben?

```
print(1)
# print(2)
print(3)
# print(4)
```

- A) 1, 2, 3, 4
- B) Keine
- C) 2, 4
- D) 1, 3

Korrekte Lösung: 