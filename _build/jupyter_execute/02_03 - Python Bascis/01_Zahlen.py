## Zahlen in Python

Zahlen funktionieren in Python ähnlich wie sonst überall auch - du kannst damit ganz normale Rechnungen berechnen.

5,(2 * 5),5

Hallo = "Steinam"


print(Hallo)


### Die Grundrechenarten

(5 + 3)
(5 - 4)
(5 * 4)
(5 / 4)

Auch die Klammersetzung funktioniert natürlich wie gewohnt :-):

print((5 + 4) * 2)

print(1.25 + 2)

print(1 + 2)

