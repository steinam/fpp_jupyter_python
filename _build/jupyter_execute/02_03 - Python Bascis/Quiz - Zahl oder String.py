## Quiz: Zahl oder String?

#### Aufgabe 1

```python
v = "Hallo Welt"
print(v)
```

Worum handelt es sich bei der Variable `v`?

- A) String (Zeichenkette)
- B) Zahl

Richtige Lösung: 

#### Aufgabe 2

```python
v = 14
print(v)
```

Worum handelt es sich bei der Variable `v`?

- A) String (Zeichenkette)
- B) Zahl

Richtige Lösung: 

#### Aufgabe 3

```python
v = str(14)
print(v)
```

Worum handelt es sich bei der Variable `v`?

- A) String (Zeichenkette)
- B) Zahl

Richtige Lösung: 

#### Aufgabe 4

Welche der folgenden Codezeilen ist ein gültiges Python-Programm, welches bei der Ausführung keinen Fehler erzeugt?

- A) `print("Dein Guthaben beträgt " & str(50) & " Cent.")`
- B) `print("Du hast nur noch " + str(10) + "€ auf dem Konto.")`
- C) `print("Du hast " + 50000 + "€ gewonnen.")`
- D) `print(String(Du hast Schulden.))`

Richtige Lösung: 