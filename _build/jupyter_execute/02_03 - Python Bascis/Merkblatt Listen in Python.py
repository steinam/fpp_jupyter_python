# Listen

Bislang haben wir in einer Variable jeweils nur ein Element (Zahl oder String) gespeichert. 

student1 = "Max"
student2 = "Monika"
student3 = "Erik"
student4 = "Franziska"

### Eine Liste erstellen
**Listen** hingegen können mehrere Elemente enthalten. Man erzeugt eine Liste und füllt sie mit Elementen wie folgt:

**Listenname = [Element1, Element2, Element3]** 

Natürlich kann eine Liste nicht nur drei, sondern beliebig viele Elemente enthalten.

Konkret kann das dann so aussehen:

students = ["Max", "Monika", "Erik", "Franziska"]

marks = [4, 3, 2, 1]

Listen kannst du wie Zahlen und Strings per print()-Befehl ausgeben.

print(students) 

In einer Liste dürfen auch Strings und Zahlen nebeneinander vorkommen, doch davon ist abzuraten! Wir werden dafür später geeignetere Strukturen kennenlernen. :-)

### Ein Element aus einer Liste auswählen
Du kannst auch auf die Elemente aus einer Liste einzeln zugreifen. Stell dir vor, dass alle Elemente in einer Liste durchnummeriert sind, aufsteigend von 0 an. Per **Index** kannst du ein Element über seine Position in der Liste anwählen: 

**Listenname[Position]**

Konkret sieht das so aus:

print(students[0])

print(students[1])

print(students[2])

print(students[3])

Die ausgewählten Elemente aus einer Liste kannst du dann so weiterverarbeiten, wie du das von Variablen schon kennst. Dabei solltest du beachten, ob es sich bei den Elementen um Zahlen oder Strings handelt.

print(students[0] + " & " + students[3])

# den Notendurchschnitt ausrechnen
print((marks[0] + marks[1] + marks[2] + marks[3]) / 4)

### Ein weiteres Element an eine Liste anhängen
Möchtest du ein weiteres Element an deine Liste anhängen, verwendest du den `append()`-Befehl. Anders als die Befehle, die du schon kennst, wie den print()-Befehl, steht append() nicht für sich, sondern mit einem Punkt _hinter_ dem Objekt, auf den append() angewendet wird: 

**Listenname.append(Element)**
 
Du wirst im Zuge dieses Kurses ganz automatisch lernen, welche Befehle für sich stehen und welche angehängt werden, und was genau das jeweils bedeutet. :-) 

students.append("Moritz")

Jetzt schauen wir uns an, ob der Befehl auch funktioniert hat ;-):

print(students)

### Die Länge einer Liste abfragen
Mit dem len-Befehl kannst du herausfinden, wie viele Elemente eine Liste enthält: **len(Listenname)**

print(len(students))

### Ein Element aus einer Liste entfernen

Um ein Element aus einer Liste zu entfernen, kannst du unter anderem die `pop()`-Funktion verwenden. Dann wird das letzte Element aus der Liste gelöscht. Die `pop()`-Funktion schreibst du wie `append()` mit einem Punkt getrennt hinter die Liste, aus der du das letzte Element entfernen möchtest.

planets = ["Erde", "Mars", "Jupiter", "Saturn", "Pluto"]

planets.pop()
print(planets)

Die Besonderheit bei `pop()` besteht darin, dass zusätzlich das gelöschte Element als Ergebnis zurückgeliefert wird.

planets = ["Erde", "Mars", "Jupiter", "Saturn", "Pluto"]

p = planets.pop()
print(p)

print(planets)