# Daten umwandeln - Strings und Zahlen


### Einen String in eine Ganzzahl umwandeln
Dazu wenden wir den **int()**-Befehl auf einen String (direkt oder in einer Variable gespeichert) an: **int(string)**

int steht für Integer, den englischen Begriff für Ganzzahl.

a = "5"
b = "6"

print(int(a) + int(b))

### Einen String in eine Kommazahl umwandeln
Dazu wenden wir den **float()**-Befehl auf einen String (direkt oder in einer Variable gespeichert) an: **float(string)** 
 
 
Der Name float kommt daher, dass man Kommazahlen auch Fließkommazahlen nennt.

a = "5.5"
b = "6.6"

print(float(a) + float(b))

### Eine Zahl in einen String umwandeln
Dazu wenden wir den **str()**-Befehl auf eine Ganzzahl oder Kommazahl an (direkt oder in einer Variable gespeichert): **str(zahl)**

age = 21
print("Ich bin " + str(age) + " Jahre alt")

### Spiel doch jetzt ein wenig herum mit dem was du gelernt hast :-) 

- Wandle Zahlen in Strings um und gebe sie als Teil eines verketteten Strings aus!
- Wandle Strings in Zahlen um, mit denen du dann rechnen kannst! 



