## Listen: Die `.pop()` - Funktion

In dieser Lektion lernst du:

- Wie du das letzte Element einer Liste entfernen kannst


Die `.pop()`-Funktion löscht das jeweils letzte Element aus einer Liste und stellt es dem Aufrufer auf Wunsch zur Verfügung

planets = ["Merkur", "Venus", "Erde", "Mars", "Jupiter", "Saturn", "Uranus", "Neptun", "Pluto"]
planets.pop()
print(planets)

planets = ["Merkur", "Venus", "Erde", "Mars", "Jupiter", "Saturn", "Uranus", "Neptun", "Pluto"]

p = planets.pop()
print(p)

print(planets)

