# Variablen

<strong>Variablen</strong> sind ein generell wesentliches Konzept von Programmiersprachen. Du kannst dir eine Variable als eine Art Behälter vorstellen, in dem du z. B. eine Zahl ablegen kannst, um sie später wiederzuverwenden. <br>

In Python kannst du eine Variable einführen und ihr einen Wert zuweisen, indem du schreibst: **Variablenname = Zahl**

Beachte, dass ein Variablenname **nicht** mit einer Zahl anfangen darf!

Konkret kann die Definition einer Variablen etwa so aussehen:

a = 5

Statt eines Wertes darf rechts vom Gleichheitszeichen auch eine Rechnung stehen, die als Ergebnis eine Zahl liefert: 

a = 5 + 6

An einer anderen Stelle im Programm greifst du dann über den Variablennamen auf die Zahl zu. Du kannst die Variable etwa in einem print()-Befehl verwenden:

print(a)

Du kannst auch mit Variablen, in denen Zahlen gespeichert sind, rechnen:

print(a * a)

b = 5
print(b * b * b)

# das Durchschnittsalter berechnen

age = 21
age2 = 18

print((age + age2) / 2)

# Statt in print() zu rechnen, können wir das Ergebnis auch erst in einer Variablen zwischenspeichern

average_age = (age + age2) / 43

print(average_age)

### Spiel doch jetzt ein wenig mit dem, was du gelernt hast, herum:
- Gib einige Rechenergebnisse per print() aus, aber speichere Zahlen und Teilergebnisse in Variablen! :-)



