# Programmierübungen

Die folgenden Übungen sollen mit Hilfe vieler kleiner Übungen ihre analytischen Fähigkeiten stärken.
Weiterhin werden Sie mit der Syntax der jeweiligen Programmiersprache vertraut

In den ersten Aufgaben geht es um folgende Fähigkeiten


- Erzeugen von Ausgaben mit der print()-Anweisung
- Einlesen von Daten incl. Umwandeln in den korrekten Datentyp
- Berechnungen mit Hilfe von float und integer-Werten unter Zuhilfenahme der folgenden Operatoren +, -, *, /, //, %, and **
- Aufruf von mathematischen Funktionen
- Formatieren der Ausgabe

## Ausgabe der Briefadresse

Erstellen Sie ein Programm, das Ihren Namen und die vollständige Postanschrift anzeigt, die in
der Art und Weise formatiert ist, wie Sie sie normalerweise auf der Außenseite eines Umschlags sehen würden. Dein Programm
muss keine Eingaben des Benutzers lesen.



## Hello

Schreibe ein Programm, das den User nach seinem Namen fragt. Das Programm sollte mit Hallo, gefolgt vom eingebenen Namen, antworten.



## Größe eines Raumes

Schreiben Sie ein Programm, das den Benutzer auffordert, die Breite und Länge eines Raums einzugeben. Sobald die Werte gelesen wurden, sollte Ihr Programm die Raumgröße berechnen und anzeigen. Die Länge und die Breite werden als Fließkommazahlen eingegeben. Fügen Sie Einheiten in Ihre Eingabeaufforderung und Ausgabemeldung ein. entweder Fuß oder Meter, je nachdem, mit welchen Maßzahlen Sie besser arbeiten können.



## Größe einer Ackerfläche



Schreibe ein Programm, das nach der Länge und Breite eines Feldes in Fuß fragt. Gib die Größe des feldes in Acre aus

Hint: There are 43,560 square feet in an acre.



## Getränkerückgabe

An einem Getränkeautomat kann man leere Behälter abgeben. Flaschen mit weniger als einem Liter erhalten 0.10€ Pfand, Flaschen mit mehr als einem Liter erhalten 0.25€ Pfand,

Das Programm soll die Anzahl der Flschen pro Größe abfragen und die Gesamtsumme des Flaschenpfandes ausgeben. Die Ausgabe soll mit einem Euro-Zeichen beginnen und auf 2 Nachkommastellen gerundet sein.



## Steuer und Trinkgeld

<!-- The program that you create for this exercise will begin by reading the cost of a meal
ordered at a restaurant from the user. Then your program will compute the tax and
tip for the meal. Use your local tax rate when computing the amount of tax owing.
Compute the tip as 18 percent of the meal amount (without the tax). The output from
your program should include the tax amount, the tip amount, and the grand total for
the meal including both the tax and the tip. Format the output so that all of the values
are displayed using two decimal places.
-->

Das Programm fragt zunächst nach dem Preis eines Essens. Anschließend berechnet es die Umsatzsteuer. Berechne einen Zuschlag von 18 % auf den Nettopreis als Trinkgeld. 

Die Ausgabe sollte den Steuerbetrag und das Trinkgeld beinhalten. Formatiere alle Ausgaben mit zwei Nachkommastellen.



## Summe von n positiven Ganzzahlen

<!--
Write a program that reads a positive integer, n, from the user and then displays the
sum of all of the integers from 1 to n. The sum of the ﬁrst n positive integers can be
computed using the formula:
-->

Ein Programm soll eine positive Ganzzahl n einlesen und die Summe aller Zahlen von 1 bis n berechnen. Dies kann mit folgender Formel geschehen:

sum = ( n )( n + 1 ) / 2



## Stoffbären und Stoffhasen

<!--
An online retailer sells two products: widgets and gizmos. Each widget weighs 75
grams. Each gizmo weighs 112 grams. Write a program that reads the number of
widgets and the number of gizmos in an order from the user. Then your program
should compute and display the total weight of the order.

-->

Ein Onlinehändler versendet zwei Produkte: Stoffbären und Stoffhasen. Ein Stoffbär wiegt 75 Gramm, ein Stoffhase 112 Gramm. Schreibe ein Programm, welches die Anzahl der jeweiligen Stofftiere einer Bestellung abfragt und daraus das Gesamtgewicht berechnet.



## Zinseszins

<!--
Pretend that you have just opened a new savings account that earns 4 percent interest
per year. The interest that you earn is paid at the end of the year, and is added
to the balance of the savings account. Write a program that begins by reading the
amount of money deposited into the account from the user. Then your program should
compute and display the amount in the savings account after 1, 2, and 3 years. Display
each amount so that it is rounded to 2 decimal places.
-->
Sie haben ein neues Guthabenkonto eröffnet, welches 1 % Zinsen abwirft. Die Zinsen werden am Ende des Jahres berechnet und dem Guthaben gutgeschrieben. 

Das Programm soll den Guthabenbetrag einlesen und dann den Gesamtbetrag nach 1,2 und 3 Jahren ausgeben. 

Jeder Betrag soll auf zwei Nachkommastellen gerundet dargestellt werden.




## Arithmetik

<!--
Create a program that reads two integers, a and b, from the user. Your program should
compute and display:

- The sum of a and b
- The difference when b is subtracted from a
- The product of a and b
- The quotient when a is divided by b
- The remainder when a is divided by b
- The result of log10a
- The result of a^b

Hint: You will probably ﬁnd the log10 function in the math module helpful for computing the second last item in the list.
-->

Schreibe ein Programm, welches zwei Integerzahlen einliest. Es sollte folgende Berechnungen durchführen und ausgeben

- Die Summe von a und b
- Die Differenz von a - b
- Das Produkt von a und b
- Den Quotienten von a : b
- Den Rest von a : b
- Das Ergebnis von a^b




## Benzinverbrauch

<!--
In the United States, fuel efﬁciency for vehicles is normally expressed in miles-per-gallon (MPG). In Canada, fuel efﬁciency is normally expressed in liters-per-hundred kilometers (L/100km). Use your research skills to determine how to convert from MPG to L/100km. Then create a program that reads a value from the user in American units and displays the equivalent fuel efﬁciency in Canadian units.
-->
In den USA wird der Benzinverbrauch in miles-per-gallon (MPG) gemessen. In Europ wird dies mit Liter-auf-100-Kilometer (L/100km) angegeben. Finden Sie heraus wie man MPG nach L/100km umrechnet. Schreiben Sie dann ein Programm, das einen Wert in US-Angaben erhält und es entspricht in europ. Verhältnisse umrechnet.









## Wechselgeld

Schreiben Sie die Routine zur Berechnung und Ausgabe des Rückgabegeldes in einem Parkhaus. 
Die Maschine muss das Rückgabegeld berechnen sowie eine möglichst geringe Ausgabe von Münzen erreichen.
Die Parkgebühren werden in 10 Cent-Schritten berechnet, d.h. eine Parkgebühr von bspw. 10,24 € kann nicht vorkommen. Weiterhon können nur Münzen ab 10 Cent-Wert in den Parkautomaten eingeführt werden.

Zur Eingabe der Parkhausgebühren und des vom Nutzer gegebenen Geldes benutzen Sie bitte zwei input-Felder.

Das Programm berechnet dann die Differenz und die Anzahl der verschiedenen Münzen, die in den Ausgabeschacht fallen sollen. Die Kombination der Münzen soll so sein, dass die maximal geringste Menge an Münzen zurückgegenen wird. 

Im Euroraum stehen Ihnen in Parkhäusern folgende Münzen zur Verfügung:

- 10, 20, 50 Cent 
- 1, 2 Euro



## Body Mass Index

Write a program that computes the body mass index (BMI) of an individual. Your
program should begin by reading a height and weight from the user. Then it should

use one of the following two formulas to compute the BMI before displaying it. If
you read the height in inches and the weight in pounds then body mass index is
computed using the following formula:


BMI = (weight/(height × height)) × 703 .

If you read the height in meters and the weight in kilograms then body mass index is computed using this slightly simpler formula:

BMI = weight / (height × height)



## Distanz zwischen zwei Punkten der Erde
   
Die Oberfläche der Erde ist gekrümmt und die Entfernung zwischen Längengraden variiert mit den Breitengraden. Deshalb ist die Distanz zwischen zwei Punkten der Oberfläche etwas komplizierter auszurechen als mit dem Satz des Pythagoras.


Sind (t1, g1) und (t2, g2) die Breiten- und Längengrade zweier Punkte der Erde. Dann ist die Distanz zwischen diesen Punkten wie folgt zu berechnen.

distance = 6371.01 × arccos ( sin ( t1 ) × sin ( t2 ) + cos ( t1 ) × cos ( t2 ) × cos ( g1 − g2 ))

Der Wert 6371.01 ist nicht zufällig, sondern der durchschnittliche Radius der Erde in Kilometern.

Erzeuge ein Programm, dass es dem Anwender erlaubt, die Breiten- und Längengrade zweier Punkte einzugeben. Es sollte dann die Distanz zwischen diesen Punkten in Kilometern ausgeben.

**Hinweis:**
Python’s trigonometric functions operate in radians. As a result, you will
need to convert the user’s input from degrees to radians before computing the
distance with the formula discussed previously. The math module contains a
function named radians which converts from degrees to radians.





## Height Units

Many people think about their height in feet and inches, even in some countries that
primarily use the metric system. Write a program that reads a number of feet from
the user, followed by a number of inches. Once these values are read, your program
should compute and display the equivalent number of centimeters.
Hint: One foot is 12 inches. One inch is 2.54 centimeters.




## Distance Units 

In this exercise, you will create a program that begins by reading a measurement
in feet from the user. Then your program should display the equivalent distance in
inches, yards and miles. Use the Internet to look up the necessary conversion factors
if you don’t have them memorized.




## Area and Volume

Write a program that begins by reading a radius, r, from the user. The program will
continue by computing and displaying the area of a circle with radius r and the
volume of a sphere with radius r. Use the pi constant in the math module in your
calculations.

Hint: The area of a circle is computed using the formula area = π * r ^ 2

The volume of a sphere is computed using the formula 

volume = 4/3 * π *r^3





## Heat Capacity

The amount of energy required to increase the temperature of one gram of a material
by one degree Celsius is the material’s speciﬁc heat capacity, C. The total amount
of energy required to raise m grams of a material by Δ T degrees Celsius can be
computed using the formula:

q = mC Δ T .
Write a program that reads the mass of some water and the temperature change
from the user. Your program should display the total amount of energy that must be
added or removed to achieve the desired temperature change.
Hint: The speciﬁc heat capacity of water is 

4.186 * J/g°^C

Because water has a density of 1.0 gram per millilitre, you can use grams and millilitres interchangeably
in this exercise.
Extend your program so that it also computes the cost of heating the water. Elec-
tricity is normally billed using units of kilowatt hours rather than Joules. In this
exercise, you should assume that electricity costs 8.9 cents per kilowatt-hour. Use
your program to compute the cost of boiling water for a cup of coffee.




## Volume of a Cylinder 

The volume of a cylinder can be computed by multiplying the area of its circular
base by its height. Write a program that reads the radius of the cylinder, along with
its height, from the user and computes its volume. Display the result rounded to one
decimal place.




## Free Fall

Create a program that determines how quickly an object is traveling when it hits the
ground. The user will enter the height from which the object is dropped in meters (m).
Because the object is dropped its initial speed is 0m/s. Assume that the acceleration
due to gravity is 

9.8m / s^2. 

You can use the formula vf = ..... has to be done

to compute the ﬁnal speed, vf , when the initial speed, vi , acceleration, a, and distance, d, are known.




## Ideal Gas Law 


The ideal gas law is a mathematical approximation of the behavior of gasses as
pressure, volume and temperature change. It is usually stated as:
PV = nRT
where P is the pressure in Pascals, V is the volume in liters, n is the amount of
substance in moles, R is the ideal gas constant, equal to 8.314
J
mol K
, and T is the
temperature in degrees Kelvin.
Write a program that computes the amount of gas in moles when the user supplies
the pressure, volume and temperature. Test your program by determining the number
of moles of gas in a SCUBA tank. A typical SCUBA tank holds 12 liters of gas at
a pressure of 20,000,000 Pascals (approximately 3,000 PSI). Room temperature is
approximately 20 degrees Celsius or 68 degrees Fahrenheit.

Hint: 

A temperature is converted from Celsius to Kelvin by adding 273.15 to it. To convert a temperature from Fahrenheit to Kelvin, deduct 32 from it, multiply it by 5/9 and then add 273.15 to it.



## Area of a Triangle 

The area of a triangle can be computed using the following formula, where b is the length of the base of the triangle, and h is its height:

area = (b × h) / 2

Write a program that allows the user to enter values for b and h. The program should then compute and display the area of a triangle with base length b and height h.




## Area of a Triangle (Again) 

In the previous exercise you created a program that computed the area of a triangle
when the length of its base and its height were known. It is also possible to compute
the area of a triangle when the lengths of all three sides are known. Let s1, s2 and s3
be the lengths of the sides. Let s = ( s1 + s2 + s3 )/ 2. Then the area of the triangle
can be calculated using the following formula:

area = sqrt(s × ( s − s1 ) × ( s − s2 ) × ( s − s3 ))

Develop a program that reads the lengths of the sides of a triangle from the user and displays its area.



## Area of a Regular Polygon 


A polygon is regular if its sides are all the same length and the angles between all of
the adjacent sides are equal. The area of a regular polygon can be computed using
the following formula, where s is the length of a side and n is the number of sides:


area = (n × s^2) / 4 × tan (π/n)

Write a program that reads s and n from the user and then displays the area of a
regular polygon constructed from these values.



## Units of Time 

Create a program that reads a duration from the user as a number of days, hours,
minutes, and seconds. Compute and display the total number of seconds represented
by this duration.




## Units of Time (Again) 


In this exercise you will reverse the process described in the previous exercise.
Develop a program that begins by reading a number of seconds from the user.
Then your program should display the equivalent amount of time in the form
D:HH:MM:SS, where D, HH, MM, and SS represent days, hours, minutes and sec-
onds respectively. The hours, minutes and seconds should all be formatted so that
they occupy exactly two digits, with a leading 0 displayed if necessary.




## Current Time

Python includes a library of functions for working with time, including a function
called asctime in the time module. It reads the current time from the com-
puter’s internal clock and returns it in a human-readable format. Write a program
that displays the current time and date. Your program will not require any input from
the user.






## Wind Chill


When the wind blows in cold weather, the air feels even colder than it actually is
because the movement of the air increases the rate of cooling for warm objects, like
people. This effect is known as wind chill.
In 2001, Canada, the United Kingdom and the United States adopted the fol-
lowing formula for computing the wind chill index. Within the formula Ta is the
air temperature in degrees Celsius and V is the wind speed in kilometers per hour.
A similar formula with different constant values can be used with temperatures in
degrees Fahrenheit and wind speeds in miles per hour.

WCI = 13.12 + 0.6215Ta − 11.37V^0.16 + 0.3965TaV^0.16

Write a program that begins by reading the air temperature and wind speed from the
user. Once these values have been read your program should display the wind chill
index rounded to the closest integer.
The wind chill index is only considered valid for temperatures less than or
equal to 10 degrees Celsius and wind speeds exceeding 4.8 kilometers per
hour.




## Celsius to Fahrenheit and Kelvin 


Write a program that begins by reading a temperature from the user in degrees
Celsius. Then your program should display the equivalent temperature in degrees
Fahrenheit and degrees Kelvin. The calculations needed to convert between different
units of temperature can be found on the internet.



## Units of Pressure 

In this exercise you will create a program that reads a pressure from the user in kilo-
pascals. Once the pressure has been read your program should report the equivalent
pressure in pounds per square inch, millimeters of mercury and atmospheres. Use
your research skills to determine the conversion factors between these units.






## Sum of the Digits in an Integer 

Develop a program that reads a four-digit integer from the user and displays the sum
of the digits in the number. For example, if the user enters 3141 then your program
should display 3 + 1 + 4 + 1 = 9.




## Sort 3 Integers

Create a program that reads three integers from the user and displays them in sorted
order (from smallest to largest). Use the min and max functions to ﬁnd the smallest
and largest values. The middle value can be found by computing the sum of all three
values, and then subtracting the minimum value and the maximum value.




## Day Old Bread 
A bakery sells loaves of bread for $3.49 each. Day old bread is discounted by 60
percent. Write a program that begins by reading the number of loaves of day old
bread being purchased from the user. Then your program should display the regular
price for the bread, the discount because it is a day old, and the total price. All of the
values should be displayed using two decimal places, and the decimal points in all
of the numbers should be aligned when reasonable values are entered by the user.

