# Daten umwandeln - Listen und Strings
Wir können mit Python Elemente zu einer Liste zusammenfügen oder eine Liste in einzelne Elemente zerlegen.

### Strings aus einer Liste zu einem String zusammenfügen

Mit dem  **join()**-Befehl, der auf einen String angewendet wird, verbinden wir die Strings aus einer Liste zu einem neuen String: **string.join(liste)**

Der String, auf den join() angewendet wird, bildet dabei die Nahtstelle: Dieser String wird als Verbindung zwischen den einzelnen Listenelementen im neuen String gesetzt.

students = ["Max", "Monika", "Erik", "Franziska"]
print(", ".join(students))

students_as_string = ", ".join(students)
print("An unserer Uni studieren: " + students_as_string)

students = ["Max", "Monika", "Erik", "Franziska"]
print(" - ".join(students))

### Einen String in eine Liste aufspalten

Mit dem **split()**-Befehl, der auf einen String angewendet wird, wird dieser String an seinen Leerzeichen aufgespalten und die daraus resultierenden Einzelstrings in einer Liste gespeichert: **string.split()**

i = "Max, Monika, Erik, Franziska"

print(i.split())

Wir können sogar noch genauer festlegen, an welchen Stellen der String von split() aufgespaltet werden soll.

print(i.split(", "))

print(i.split("a"))

Insbesondere können wir auch mehrere der Befehle, die wir schon kennen gelernt haben, miteinander kombinieren:

# Hier zählen wir die Anzahl der Wörter des Satzes s

s = "Ich bin ein Satz mit vielen Wörtern"
print(len(s.split()))

### Spiel doch jetzt ein wenig herum mit dem was du gelernt hast:
- Zerlege Strings in Listen mit split() und erzeuge eine Liste aus einzelnen Strings per join(). :-)
- Du kannst auch versuchen, die Funktionen, die du schon kennst, miteinander zu kombinieren.  :-)




