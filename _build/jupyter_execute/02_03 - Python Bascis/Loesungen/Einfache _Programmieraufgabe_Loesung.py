# Introduction to Programming Exercices

The exercises in this chapter are designed to help you develop your analysis skills by providing you with the opportunity to practice breaking small problems down into sequences of steps. 

In addition, completing these exercises will help you become familiar with Python’s syntax. To complete each exercise you should expect to use some or all of these Python features:

- Generate output with print statements
- Read input, including casting that input to the appropriate type
- Perform calculations involving integers and ﬂoating point numbers using operators like +, -, *, /, //, %, and **
- Call functions residing in the math module
- Control how output is displayed using format speciﬁers

## Mailing Adress

Create a program that displays your name and complete mailing address formatted in
the manner that you would usually see it on the outside of an envelope. Your program
does not need to read any input from the user.

print("Michéal O'Dwyer")
print("In A Galaxy Far Far Away")
print("All The Way In")
print("Boise, Idaho,")
print("U.S.A".format())

## Hello

Write a program that asks the user to enter his or her name. The program should
respond with a message that says hello to the user, using his or her name.

name = input("Please type in your name: ")
print("Hello, {}.".format(name))

## Area of a room

Write a program that asks the user to enter the width and length of a room. Once
the values have been read, your program should compute and display the area of the
room. The length and the width will be entered as ﬂoating point numbers. Include
units in your prompt and output message; either feet or meters, depending on which
unit you are more comfortable working with.



## Area of a field

Create a program that reads the length and width of a farmer’s ﬁeld from the user in
feet. Display the area of the ﬁeld in acres.

Hint: There are 43,560 square feet in an acre.



## Bottle Deposits

In many jurisdictions a small deposit is added to drink containers to encourage people
to recycle them. In one particular jurisdiction, drink containers holding one liter or
less have a $0.10 deposit, and drink containers holding more than one liter have a
$0.25 deposit.
Write a program that reads the number of containers of each size from the user.
Your program should continue by computing and displaying the refund that will be
received for returning those containers. Format the output so that it includes a dollar
sign and always displays exactly two decimal places.




## Tax and Tips

The program that you create for this exercise will begin by reading the cost of a meal
ordered at a restaurant from the user. Then your program will compute the tax and
tip for the meal. Use your local tax rate when computing the amount of tax owing.
Compute the tip as 18 percent of the meal amount (without the tax). The output from
your program should include the tax amount, the tip amount, and the grand total for
the meal including both the tax and the tip. Format the output so that all of the values
are displayed using two decimal places.



## Sum of the first n positive Integers

Write a program that reads a positive integer, n, from the user and then displays the
sum of all of the integers from 1 to n. The sum of the ﬁrst n positive integers can be
computed using the formula:

sum = ( n )( n + 1 ) / 2



## Widgets and Gizmos

An online retailer sells two products: widgets and gizmos. Each widget weighs 75
grams. Each gizmo weighs 112 grams. Write a program that reads the number of
widgets and the number of gizmos in an order from the user. Then your program
should compute and display the total weight of the order.



## Compound Interest

Pretend that you have just opened a new savings account that earns 4 percent interest
per year. The interest that you earn is paid at the end of the year, and is added
to the balance of the savings account. Write a program that begins by reading the
amount of money deposited into the account from the user. Then your program should
compute and display the amount in the savings account after 1, 2, and 3 years. Display
each amount so that it is rounded to 2 decimal places.



## Aritmetic

Create a program that reads two integers, a and b, from the user. Your program should
compute and display:

- The sum of a and b
- The difference when b is subtracted from a
- The product of a and b
- The quotient when a is divided by b
- The remainder when a is divided by b
- The result of log10a
• The result of a^b

Hint: You will probably ﬁnd the log10 function in the math module helpful for computing the second last item in the list.



## Fuel Efficency

In the United States, fuel efﬁciency for vehicles is normally expressed in miles-per-gallon (MPG). In Canada, fuel efﬁciency is normally expressed in liters-per-hundred kilometers (L/100km). Use your research skills to determine how to convert from MPG to L/100km. Then create a program that reads a value from the user in American units and displays the equivalent fuel efﬁciency in Canadian units.

mpgTOlp100km = 235.214583
mpg = float(input("please input your MPG"))
print("your liters per 100 km is : ", mpg*mpgTOlp100km)

MPG = float(input("How many Miles Per Gallon (MPG): "))

litresPer100 = 235.215 / MPG
print("The L/100km is {}L".format(litresPer100))

## Distance between two points on Earth
   
The surface of the Earth is curved, and the distance between degrees of longitude varies with latitude. As a result, ﬁnding the distance between two points on the surface of the Earth is more complicated than simply using the Pythagorean theorem.

Let ( t1 , g1 ) and ( t2 , g2 ) be the latitude and longitude of two points on the Earth’s surface. The distance between these points, following the surface of the Earth, in kilometers is:

distance = 6371 . 01 × arccos ( sin ( t1 ) × sin ( t2 ) + cos ( t1 ) × cos ( t2 ) × cos ( g1 − g2 ))

The value 6371.01 in the previous equation wasn’t selected at random. It is the average radius of the Earth in kilometers. 

Create a program that allows the user to enter the latitude and longitude of two points on the Earth in degrees. Your program should display the distance between the points, following the surface of the earth, in kilometers.

Hint: Python’s trigonometric functions operate in radians. As a result, you will
need to convert the user’s input from degrees to radians before computing the
distance with the formula discussed previously. The math module contains a
function named radians which converts from degrees to radians.

import math

t1 = float(input("Please enter the x value of a point on Earth in degrees: "))
t2 = float(input("Please enter the y value for that point on Earth in degrees: "))

g1 = float(input("Please enter the x value of a 2nd point on Earth in degrees: "))
g2 = float(input("Please enter the y value for that point on Earth in degrees: "))

distance = 6371.01 * math.acos(math.sin(math.radians(t1)) * math.sin(math.radians(g1)) + \
math.cos(math.radians(t1)) * math.cos(math.radians(g1)) * math.cos(math.radians(t2-g2)))

print("The distance between these two points on Earth is {}km".format(distance))

## Making Change

Consider the software that runs on a self-checkout machine. One task that it must be
able to perform is to determine how much change to provide when the shopper pays
for a purchase with cash.
Write a program that begins by reading a number of cents from the user as an
integer. Then your program should compute and display the denominations of the
coins that should be used to give that amount of change to the shopper. The change
should be given using as few coins as possible. Assume that the machine is loaded
with pennies, nickels, dimes, quarters, loonies and toonies.
A one dollar coin was introduced in Canada in 1987. It is referred to as a
loonie because one side of the coin has a loon (a type of bird) on it. The two
dollar coin, referred to as a toonie, was introduced 9 years later. It’s name is
derived from the combination of the number two and the name of the loonie. 



## Height Units

Many people think about their height in feet and inches, even in some countries that
primarily use the metric system. Write a program that reads a number of feet from
the user, followed by a number of inches. Once these values are read, your program
should compute and display the equivalent number of centimeters.
Hint: One foot is 12 inches. One inch is 2.54 centimeters.




## Distance Units . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 8

In this exercise, you will create a program that begins by reading a measurement
in feet from the user. Then your program should display the equivalent distance in
inches, yards and miles. Use the Internet to look up the necessary conversion factors
if you don’t have them memorized.




## Area and Volume. . . . . . . . . . . . . . . . . . . . . . . . . . . . 8

Write a program that begins by reading a radius, r, from the user. The program will
continue by computing and displaying the area of a circle with radius r and the
volume of a sphere with radius r. Use the pi constant in the math module in your
calculations.

Hint: The area of a circle is computed using the formula area = π * r ^ 2

The volume of a sphere is computed using the formula 

volume = 4/3 * π *r^3





## Heat Capacity . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 8

The amount of energy required to increase the temperature of one gram of a material
by one degree Celsius is the material’s speciﬁc heat capacity, C. The total amount
of energy required to raise m grams of a material by Δ T degrees Celsius can be
computed using the formula:

q = mC Δ T .
Write a program that reads the mass of some water and the temperature change
from the user. Your program should display the total amount of energy that must be
added or removed to achieve the desired temperature change.
Hint: The speciﬁc heat capacity of water is 

4.186 * J/g°^C

Because water has a density of 1.0 gram per millilitre, you can use grams and millilitres interchangeably
in this exercise.
Extend your program so that it also computes the cost of heating the water. Elec-
tricity is normally billed using units of kilowatt hours rather than Joules. In this
exercise, you should assume that electricity costs 8.9 cents per kilowatt-hour. Use
your program to compute the cost of boiling water for a cup of coffee.




## Volume of a Cylinder. . . . . . . . . . . . . . . . . . . . . . . . . 9

The volume of a cylinder can be computed by multiplying the area of its circular
base by its height. Write a program that reads the radius of the cylinder, along with
its height, from the user and computes its volume. Display the result rounded to one
decimal place.




## Free Fall. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 9

Create a program that determines how quickly an object is traveling when it hits the
ground. The user will enter the height from which the object is dropped in meters (m).
Because the object is dropped its initial speed is 0m/s. Assume that the acceleration
due to gravity is 

9.8m / s^2. 

You can use the formula vf = ..... has to be done

to compute the ﬁnal speed, vf , when the initial speed, vi , acceleration, a, and distance, d, are known.




## Ideal Gas Law . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 9


The ideal gas law is a mathematical approximation of the behavior of gasses as
pressure, volume and temperature change. It is usually stated as:
PV = nRT
where P is the pressure in Pascals, V is the volume in liters, n is the amount of
substance in moles, R is the ideal gas constant, equal to 8.314
J
mol K
, and T is the
temperature in degrees Kelvin.
Write a program that computes the amount of gas in moles when the user supplies
the pressure, volume and temperature. Test your program by determining the number
of moles of gas in a SCUBA tank. A typical SCUBA tank holds 12 liters of gas at
a pressure of 20,000,000 Pascals (approximately 3,000 PSI). Room temperature is
approximately 20 degrees Celsius or 68 degrees Fahrenheit.

Hint: 

A temperature is converted from Celsius to Kelvin by adding 273.15 to it. To convert a temperature from Fahrenheit to Kelvin, deduct 32 from it, multiply it by 5/9 and then add 273.15 to it.



## Area of a Triangle . . . . . . . . . . . . . . . . . . . . . . . . . . . 10

The area of a triangle can be computed using the following formula, where b is the length of the base of the triangle, and h is its height:

area = (b × h) / 2

Write a program that allows the user to enter values for b and h. The program should then compute and display the area of a triangle with base length b and height h.




## Area of a Triangle (Again) . . . . . . . . . . . . . . . . . . . . . 10

In the previous exercise you created a program that computed the area of a triangle
when the length of its base and its height were known. It is also possible to compute
the area of a triangle when the lengths of all three sides are known. Let s1, s2 and s3
be the lengths of the sides. Let s = ( s1 + s2 + s3 )/ 2. Then the area of the triangle
can be calculated using the following formula:

area = sqrt(s × ( s − s1 ) × ( s − s2 ) × ( s − s3 ))

Develop a program that reads the lengths of the sides of a triangle from the user and displays its area.



## Area of a Regular Polygon . . . . . . . . . . . . . . . . . . . . . 10


A polygon is regular if its sides are all the same length and the angles between all of
the adjacent sides are equal. The area of a regular polygon can be computed using
the following formula, where s is the length of a side and n is the number of sides:


area = (n × s^2) / 4 × tan (π/n)

Write a program that reads s and n from the user and then displays the area of a
regular polygon constructed from these values.



## Units of Time . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 11

Create a program that reads a duration from the user as a number of days, hours,
minutes, and seconds. Compute and display the total number of seconds represented
by this duration.




## Units of Time (Again) . . . . . . . . . . . . . . . . . . . . . . . . 11


In this exercise you will reverse the process described in the previous exercise.
Develop a program that begins by reading a number of seconds from the user.
Then your program should display the equivalent amount of time in the form
D:HH:MM:SS, where D, HH, MM, and SS represent days, hours, minutes and sec-
onds respectively. The hours, minutes and seconds should all be formatted so that
they occupy exactly two digits, with a leading 0 displayed if necessary.




## Current Time. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 11

Python includes a library of functions for working with time, including a function
called asctime in the time module. It reads the current time from the com-
puter’s internal clock and returns it in a human-readable format. Write a program
that displays the current time and date. Your program will not require any input from
the user.




## Body Mass Index. . . . . . . . . . . . . . . . . . . . . . . . . . . .

Write a program that computes the body mass index (BMI) of an individual. Your
program should begin by reading a height and weight from the user. Then it should

use one of the following two formulas to compute the BMI before displaying it. If
you read the height in inches and the weight in pounds then body mass index is
computed using the following formula:


BMI = (weight/(height × height)) × 703 .

If you read the height in meters and the weight in kilograms then body mass index is computed using this slightly simpler formula:

BMI = weight / (height × height)



## Wind Chill . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 12


When the wind blows in cold weather, the air feels even colder than it actually is
because the movement of the air increases the rate of cooling for warm objects, like
people. This effect is known as wind chill.
In 2001, Canada, the United Kingdom and the United States adopted the fol-
lowing formula for computing the wind chill index. Within the formula Ta is the
air temperature in degrees Celsius and V is the wind speed in kilometers per hour.
A similar formula with different constant values can be used with temperatures in
degrees Fahrenheit and wind speeds in miles per hour.

WCI = 13.12 + 0.6215Ta − 11.37V^0.16 + 0.3965TaV^0.16

Write a program that begins by reading the air temperature and wind speed from the
user. Once these values have been read your program should display the wind chill
index rounded to the closest integer.
The wind chill index is only considered valid for temperatures less than or
equal to 10 degrees Celsius and wind speeds exceeding 4.8 kilometers per
hour.




## Celsius to Fahrenheit and Kelvin . . . . . . . . . . . . . . . . . 12


Write a program that begins by reading a temperature from the user in degrees
Celsius. Then your program should display the equivalent temperature in degrees
Fahrenheit and degrees Kelvin. The calculations needed to convert between different
units of temperature can be found on the internet.



## Units of Pressure . . . . . . . . . . . . . . . . . . . . . . . . . . . . 13

In this exercise you will create a program that reads a pressure from the user in kilo-
pascals. Once the pressure has been read your program should report the equivalent
pressure in pounds per square inch, millimeters of mercury and atmospheres. Use
your research skills to determine the conversion factors between these units.






## Sum of the Digits in an Integer . . . . . . . . . . . . . . . . . . 13

Develop a program that reads a four-digit integer from the user and displays the sum
of the digits in the number. For example, if the user enters 3141 then your program
should display 3 + 1 + 4 + 1 = 9.




## Sort 3 Integers. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 13

Create a program that reads three integers from the user and displays them in sorted
order (from smallest to largest). Use the min and max functions to ﬁnd the smallest
and largest values. The middle value can be found by computing the sum of all three
values, and then subtracting the minimum value and the maximum value.




## Day Old Bread . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

A bakery sells loaves of bread for $3.49 each. Day old bread is discounted by 60
percent. Write a program that begins by reading the number of loaves of day old
bread being purchased from the user. Then your program should display the regular
price for the bread, the discount because it is a day old, and the total price. All of the
values should be displayed using two decimal places, and the decimal points in all
of the numbers should be aligned when reasonable values are entered by the user.

