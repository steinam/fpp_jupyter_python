## Listen verschachteln

In Python ist es erlaubt, Listen ineinander zu verschachteln. Das erlaubt uns z. B. eine Matrix zu modellieren.

In dieser Lektion lernst du:

- Wie du Listen ineinander verschachtelst
- und auf die Elemente dann entsprechend zugreifst.

liste = [
    ["Berlin", "München", "Köln"],
    ["Budapest", "Pécs", "Sopron"]
]

liste[0][0]

## Listen in Dictionaries

students = {
    "Informatik": ["Max", "Monika"],
    "BWL": ["Erik", "Franziska"]
}

print(students["Informatik"])
print(students["BWL"])

