# Zugriff auf Listeninhalte

Der Zugriff auf die Inhalte erfolt über die Indexposition, Gezählt wird mit o. 

students = ["Max", "Monika", "Erik", "Franziska", "ABC"]

print(students[-1])
print(students[-2])

## List Slicing

print(students[2:4])

print(students[1:-1])

print(students[0:-1])
print(students[:-1])

print(students[1:])

### List Slicing funktioniert auf auf Strings!

print("Hallo Welt"[0:5])

print("Hallo Welt"[-4:])

print("Hallo Welt"[1:5])

