## Dictionaries und Schleifen

In dieser Lektion lernst du:

- Wie du ein Dictionary Eintrag für Eintrag durchgehen kannst
- Und wie du hierfür das Entpacken von Tupeln benötigst :)

d = {"München": "MUC", "Budapest": "BUD", "Helsinki": "HEL"}

for key in d: 
    value = d[key]
    print(key)
    print(value)

for key, value in d.items():
    print(key + ": " + value)

