## List Comprehensions

In dieser Lektion lernst du:

- List Comprehensions
- Wie du damit Listen einfacher in eine neue Liste umwandeln kannst
- Und wir nutzen eine List Comprehension, um einfacher eine Grafik zu zeichnen

siehe auch: https://www.data-science-architect.de/listen-python/




xs = [1, 2, 3, 4, 5, 6, 7, 8]

#Nultipliziere alle Elemente der Liste mit dem Wert 2

result = []

for i in xs:
    result.append(i * 2)
    
print(result)


xs = [1, 2, 3, 4, 5, 6, 7, 8]

#List comprehension



ys = [x * x for x in xs]
# ys = []
# for x in xs:
#    ys.append(x * x)
    
print(xs)
print(ys)

### List Comprehensions können aber noch viel mehr!

students = ["Max", "Monika", "Erik", "Franziska"]

lengths = [len(student) for student in students]

#lengths = []
#for student in students:
#    lengths.append(len(student))
    
print(lengths)

### Praktisch auch für's Zeichnen von Grafiken!

%matplotlib inline
import matplotlib.pyplot as plt

xs = [x / 10 for x in range(0, 100)]
ys = [x * x for x in xs]

print(xs)
print(ys)

plt.plot(xs, ys)
plt.show()

