## Quiz Dictionaries

#### Aufgabe 1

Welcher Code definiert ein Dictionary?

- A) `["Trump": "USA", "Merkel": "Deutschland"]`
- B) `("Trump": "USA", "Merkel": "Deutschland")`
- C) `{"Trump", "USA", "Merkel", "Deutschland"}`
- d) `{"Trump": "USA", "Merkel": "Deutschland"}`

Richtige Antwort: 

#### Aufgabe 2

Wir haben ein Dictionary wie folgt definiert:

```python
d = {"Trump": "USA", "Merkel": "Deutschland"}
```

Wie greifen wir jetzt auf den ersten Eintrag (Trump / USA) zu?

- A) `print(d["Trump"])`
- B) `print(d["USA"])`
- C) `print(d[0])`
- D) `print(d[1])`

Richtige Antwort: 

#### Aufgabe 3

Wir haben ein Dictionary wie folgt definiert:

```python
d = {"Trump": "USA", "Merkel": "Deutschland"}
```

Wie überprüfst du, ob der Eintrag (Trump / USA) im Dictionary enthalten ist?

- A) `0 in d`
- B) `"Trump": "USA" in d`
- C) `"Trump" in d`
- D) `"USA" in d`

Richtige Antwort: 