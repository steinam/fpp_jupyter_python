## Aufgabe: Dictionaries

**Aufgabe:**

Lese die ../data/names.csv - Datei ein und berechne, welcher Name insgesamt in den gesamten USA am häufigsten vergeben wurde. 

**Tipps**:

- Lies zuerst die Daten in ein Dictionary ein und zähle, wie oft jeder Vorname insgesamt vorgekommen ist.
- Analysiere dann erst das Dictionary und finde den häufigsten Vornamen heraus.
- Achte drauf, wenn du 2 Zahlen addieren möchtest, musst du ggf. einen String zuerst in eine Zahl umwandeln.
- Schreibe den gesamten Code, der die Datei öffnet und durchgeht, in einer Zelle.



with open("../data/names.csv", "r") as file:
    for line in file:
        print(line)
        break

