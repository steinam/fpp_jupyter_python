## Listen

Die *Liste* in Python ähnelt dem Konzept des *Arrays* in anderen Programmiersprachen, ergänzt um interessante zusätzliche Aspekte.

Darüber hinaus gibt es noch das Konzept des

- **Tupel**: Unveränderlicher Array
- **Dictionary**: Key-Value-Store (Hashtable)

Listen ermöglichen es dir, direkt mehrere Einträge auf einmal zu speichern. Hier z. B. erfassen wir eine Liste von 4 Studierenden:

students = ["Max", "Monika", "Erik", "Franziska"]

last_student = students.pop()
print(last_student)
print(students)

Über den `+` - Operator kannst du 2 Listen miteinander verknüpfen!

students = ["Max", "Monika", "Erik", "Franziska"] + ["ABCDEF"]
print(students)

Der `del` - Befehl entfernt einen Eintrag aus einer Liste. Hierbei wird ein Element mit einem bestimmten Index (hier: 4. Element, weil das 4. Element wird über `[3]` angesprochen, weil ja bei 0 angefangen wird zu zählen):

students = ["Max", "Monika", "Erik", "Franziska", "ABCDEF"]
del students[3]
print(students)

Der `.remove()` - Befehl entfernt einen Eintrag nach Wert. Sprich, hier wird dann der Eintrag "Monika" aus der Liste entfernt:

students = ["Max", "Monika", "Erik", "Franziska", "ABCDEF"]
students.remove("Monika")
print(students)

