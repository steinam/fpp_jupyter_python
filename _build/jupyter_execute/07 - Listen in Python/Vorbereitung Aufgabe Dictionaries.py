liste = ["Hallo", "Hallo", "Welt", "Hallo", "Mars"]

d = {}
for element in liste:
    if element in d:
        d[element] = d[element] + 1
    else:
        d[element] = 1
    
print(d)

max_occurences = 0

for key, value in d.items():
    if max_occurences < value:
        max_occurences = value

print(max_occurences)

