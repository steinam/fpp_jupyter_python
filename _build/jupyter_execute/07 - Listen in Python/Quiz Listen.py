## Quiz: Listen

#### Aufgabe 1

Welchen Buchstaben gibt folgendes Programm aus?

```python
print("Hallo Welt"[-2])
```

- A) `t`
- B) `l`
- C) `H`
- D) `e`

Richtige Lösung: 

#### Aufgabe 2

Was gibt folgendes Programm aus?

```python
print("Max Mustermann"[-3:])
```

- A) `Max`
- B) `Mus`
- C) `Max Musterm`
- D) `ann`

Richtige Lösung: 

#### Aufgabe 3

Was gibt folgendes Programm aus?

```python
print("Max Mustermann"[:-3])
```

- A) `Max`
- B) `Mus`
- C) `Max Musterm`
- D) `ann`

Richtige Lösung: 

#### Aufgabe 4

Was gibt folgendes Programm aus?

```python
print("Hallo Welt!"[6:-1])
```

- A) `Halt`
- B) `Welt`
- C) `Hallo`
- D) `lo Welt!`

Richtige Lösung: 



