## Tupel

Tupel sind sowas ähnliches wie eine Liste... werden aber für andere Zwecke verwendet.

- Hier geht's jetzt darum, was Tupel sind, ...
- wie sich diese von einer Liste unterschieden
- und wie du diese in Python verwenden kannst

t = (1, 2, 3)
print(t)

liste = [1, 2, 3]
liste.append(5)
print(liste)

## Exkurs Informatik: Mutable (veränderlich) vs. Immutable (unveränderlich)

- **Liste:** Veränderliche Datenstruktur
- **Tupel:** Unveränderliche Datenstruktur

person = ("Max Müller", 55)

def do_something(p):
    p[0] = "Max Mustermann"
    
do_something(person)

print(person)




