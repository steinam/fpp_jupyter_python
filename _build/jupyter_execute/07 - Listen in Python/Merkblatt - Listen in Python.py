## Listen

Listen ermöglichen es dir, direkt mehrere Einträge auf einmal zu speichern. Hier z. B. erfassen wir eine Liste von 4 Studierenden:

students = ["Max", "Monika", "Erik", "Franziska"]

last_student = students.pop()
print(last_student)
print(students)

Über den `+` - Operator kannst du 2 Listen miteinander verknüpfen!

students = ["Max", "Monika", "Erik", "Franziska"] + ["ABCDEF"]
print(students)

Der `del` - Befehl entfernt einen Eintrag aus einer Liste. Hierbei wird ein Element mit einem bestimmten Index (hier: 4. Element, weil das 4. Element wird über `[3]` angesprochen, weil ja bei 0 angefangen wird zu zählen)

students = ["Max", "Monika", "Erik", "Franziska", "ABCDEF"]
del students[3]
print(students)

Der `.remove()` - Befehl entfernt einen Eintrag nach Wert. Sprich, hier wird dann der Eintrag "Monika" aus der Liste entfernt.

students = ["Max", "Monika", "Erik", "Franziska", "ABCDEF"]
students.remove("Monika")
print(students)

## List Comprehensions

Mit Hilfe von List Comprehensions kannst du recht einfach eine Liste in eine andere Liste umwandeln:

xs = [1, 2, 3, 4, 5, 6, 7, 8]

ys = [x * x for x in xs]
# ys = []
# for x in xs:
#    ys.append(x * x)
    
print(xs)
print(ys)

### List Comprehensions können aber noch viel mehr!

students = ["Max", "Monika", "Erik", "Franziska"]

lengths = [len(student) for student in students]

#lengths = []
#for student in students:
#    lengths.append(len(student))
    
print(lengths)

### Praktisch auch für's Zeichnen von Grafiken!

%matplotlib inline
import matplotlib.pyplot as plt

xs = [x / 10 for x in range(0, 100)]
ys = [x * x for x in xs]

print(len(xs))
print(len(ys))

plt.plot(xs, ys)
plt.show()

## Listen verschachteln

In Python ist es erlaubt, Listen ineinander zu verschachteln. Das erlaubt uns z. B. eine Matrix zu modellieren:

liste = [
    ["Berlin", "München", "Köln"],
    ["Budapest", "Pécs", "Sopron"]
]

liste[0][0]

