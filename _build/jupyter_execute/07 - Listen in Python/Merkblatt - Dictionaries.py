## Dictionaries in Python

Dictionaries sind unglaublich praktisch, damit kannst du z. B. folgendes machen:

- Du kannst Wertezuordnungen speichern (z.B. Telefonbuch: Ein Nachname hat eine Telefonnummer).
- Du kannst nachträglich Elemente verändern / entfernen / hinzufügen.
- Dictionaries brauchst du wirklich immer wieder...

Machen wir mal ein Beispiel...

d = {"Berlin": "BER", "Helsinki": "HEL", "Saigon": "SGN"}

print(d)

Zugriff auf ein einzelnes Element:

print(d["Helsinki"])

Hiermit überschreibst du ein einzelnes Element:

d["Budapest"] = "BUD"

print(d)

## Element entfernen

del d["Budapest"]

print(d)

## Abfrage: Ist ein Element im Dictionary?

if "Budapest" in d:
    print("Budapest ist im Dictionary enthalten")
if "Saigon" in d:
    print("Saigon ist im Dicionary enthalten")

## Auf Elemente zugreifen...

print(d["Saigon"])
print(d.get("Saigon"))

print(d["Budapest"])

print(d.get("Budapest"))

## Dictionaries und Schleifen

Du hast bei Dictionaries 2 Möglichkeiten, diese mit einer for - Schleife durchzugehen.

Entweder direkt, dann gehst du die Schlüssel durch:

d = {"München": "MUC", "Budapest": "BUD", "Helsinki": "HEL"}

for key in d: 
    value = d[key]
    print(key)
    print(value)

Oder über die .items() - Methode, damit kannst du Schlüssel + Wert direkt durchgehen:

for key, value in d.items():
    print(key + ": " + value)

