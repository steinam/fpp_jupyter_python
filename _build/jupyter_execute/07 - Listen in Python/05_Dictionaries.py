## Dictionaries in Python

In dieser Lektion stelle ich dir Dictionaries vor. Warum benötigst du die?

- Du kannst Wertezuordnungen speichern (z. B. Telefonbuch: Ein Nachname hat eine Telefonnummer).
- Du kannst nachträglich Elemente verändern / entfernen / hinzufügen.
- Dictionaries brauchst du wirklich immer wieder...
- Wir schauen uns später hier in diesem Kurs auch noch ein paar Beispiele an!

d = {"Berlin": "BER", "Helsinki": "HEL", "Saigon": "SGN"}

print(d)

print(d["Helsinki"])

d["Budapest"] = "BUD"

print(d)

## Element entfernen

del d["Budapest"]

print(d)

## Abfrage: Ist ein Element/Key im Dictionary?

if "Budapest" in d:
    print("Budapest ist im Dictionary enthalten")
if "Saigon" in d:
    print("Saigon ist im Dicionary enthalten")
     
print("Berlin" in d)

vals = d.values()
print(vals)

vals1 = d.keys()
print(vals1)




## Auf Elemente zugreifen...

print(d["Saigon"])
print(d.get("Saigon"))

print(d["Budapest"])

print(d.get("Budapest"))

