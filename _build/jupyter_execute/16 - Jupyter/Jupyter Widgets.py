# Jupter Widgets: Erstelle interaktive Notebooks

In dieser Lektion lernst du:

- Wie du Texteingabefelder, Buttons und Slider erstellst.
- Link: https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html#Complete-list

import ipywidgets as widgets
from IPython.display import display

widgets.Button(description="Hallo Welt")

widgets.Text(description="Beschriftung:", value="123")

widgets.IntText(description="Beschriftung:", value=123)

widgets.FloatText(description="Beschriftung:", value=123.5)

widgets.Checkbox(description="Beschriftung:", value=False)

widgets.RadioButtons(
    options=['München', 'Berlin', 'Köln'],
    description='Welche Stadt?:',
    disabled=False
)

widgets.Dropdown(
    options=['München', 'Berlin', 'Köln'],
    description='Welche Stadt?:',
    disabled=False
)

