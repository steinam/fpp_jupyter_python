# Jupter Widgets: Erstelle interaktive Notebooks

In dieser Lektion lernst du, wie du:

- Texteingabefelder, Buttons und Slider erstellst
- Und mit denen interagieren kannst.

import ipywidgets as widgets
from IPython.display import display

age = widgets.IntText(description="Alter:", value=25)
display(age)

print(age.value)

button = widgets.Button(description="OK")
display(button)

def on_button_click(p):
    print("on_button_click()")

# Wenn geklickt wird: on_button_click(button)
button.on_click(on_button_click)

age = widgets.IntText(description="Alter:", value=25)
display(age)
button = widgets.Button(description="OK")
display(button)

def on_button_click(p):
    print(age.value)

# Wenn geklickt wird: on_button_click(button)
button.on_click(on_button_click)

