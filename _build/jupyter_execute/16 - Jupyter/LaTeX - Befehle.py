In dieser Lektion geht's darum, wie du mathematische Formeln in das Jupyter Notebook einbinden kannst.

Links:

- LaTex Math: https://en.wikibooks.org/wiki/LaTeX/Mathematics
- Mathematische Zeichen: http://oeis.org/wiki/List_of_LaTeX_mathematical_symbols

Normaler Text...

$ \alpha < b $

$ a^2 < \sqrt{b} $

Der Mathematische Ausdruck: $ k_{n+1} = n^2 + k_n^2 - k_{n-1} $

Der Mathematische Ausdruck: $$ \frac{n!}{k!(n-k)!} = \binom{n}{k} $$



