## Exkurs: CSV - Datei schreiben

In dieser Lektion gehen wir nochmal kurz darauf ein, wie du mit Python eine .csv-Datei erstellen kannst.

(Link: https://docs.python.org/3.6/library/csv.html)

import csv
with open('students.csv', 'a', newline='') as f:
    writer = csv.writer(f, delimiter=";")
    
    writer.writerow(["Spalte 1", "Spalte 2", "Spalte 3"])
    writer.writerow(["Zeile 2: Spalte 1", "Spalte 2", "Spalte 3"])

