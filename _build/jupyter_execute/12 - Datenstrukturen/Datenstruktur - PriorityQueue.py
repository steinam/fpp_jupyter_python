## Weitere Datenstruktur: PriorityQueue

In dieser Lektion lernst du eine weitere Datenstruktur kennen: PriorityQueue.

Das ist eine Warteschlange, die die Einträge für uns automatisch sortiert!

import queue
q = queue.PriorityQueue()

q.put((10, "Hallo Welt"))
q.put((15, "Mars"))
q.put((5, "Wichtig"))

q.get()

q.get()

text = "A A A A A B B B C C C C C D D D D D D D D E E E E E E E"
d = {}
for word in text.split(" "):
    if word in d:
        d[word] = d[word] + 1
    else:
        d[word] = 1

pq = queue.PriorityQueue()
for word, number in d.items():
    pq.put((-number, word))

print(pq.get())
print(pq.get())
print(pq.get())

