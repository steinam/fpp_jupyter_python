## Aufgabe: Sets

Den Umgang mit einem Set möchten wir jetzt natürlich etwas üben. ;-)

**Aufgabe:**

Öffne die ../data/names.csv - Datei als .csv-Datei und berechne die Anzahl der verschiedenen Vornamen, die in dieser Datei aufgelistet sind!

**Tipps:**

- Die Dokumentation zum csv-Modul von Python findest du hier: https://docs.python.org/3.6/library/csv.html.
- Verwende dazu ein Set. Damit geht das ganze ziemlich easy. :-)

import csv

names = set()

with open('../data/names.csv', newline='') as csvfile:
    namereader = csv.reader(csvfile, delimiter=',', quotechar='"')
    counter = 0
    for row in namereader:
        if counter != 0:
            names.add(row[1])
        counter = counter + 1
print(len(names))



