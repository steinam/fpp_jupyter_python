## Aufgabe: PriorityQueue


Ermittle den 5.-häufigsten Vornamen, der in den USA vergeben wurde! Lies dazu die ../data/names.csv - Datei ein. Verwende dazu zuerst ein Dictionary, mit dem du die Häufigkeit der Vornamen zählst und anschließend eine PriorityQueue, um die Top 5 Vornamen zu ermitteln.

