## Weitere Datenstruktur: Queue

In dieser Lektion lernst du eine weitere Datenstruktur kennen: Queue.

Diese ist für folgende Operationen optiminiert:

- Eintrag hinzufügen
- Eintrag vom Anfang entfernen.

import queue
q = queue.Queue()

q.put("Hallo")
q.put("Welt")
q.put("Hallo")
q.put("Mars")
q.put("Pluto")

print(q)

print(q.get())

print(q.get())

while not q.empty():
    element = q.get()
    print(element)

