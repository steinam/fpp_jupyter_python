## Datenstrukturen in Python

Du hast jetzt schon 2 Datenstrukturen in Python kennengelernt:

- Listen: `liste = ["Hallo", "Welt"]`
- Dictionaries: `d = {"Hallo": 5, "Welt": 4}`

Es gibt aber auch noch weitere... In dieser Lektion lernst du das Set kennen!

s = ["Hallo", "Welt"]
s.append("Mars")
print(s)

s = {"Hallo", "Welt"}
s.add("Mars")
print(s)

s.add("Mars")
print(s)

if "Mars" in s:
    print("Mars ist im Set")

text = "Hallo Welt Hallo Mars Hallo Welt"
words = set()
for word in text.split(" "):
    words.add(word)
print(words)
print(len(words))

