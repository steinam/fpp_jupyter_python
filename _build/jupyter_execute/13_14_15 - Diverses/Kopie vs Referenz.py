## Wie werden in Python Funktionsparameter übergeben?

Man unterscheidet in Python zwischen 2 verschiedenen Arten von Variablen:

- Primitive Datentypen (Zahl, String, Booleans, ...)
- Datenstrukturen / Datenstruktur.

a = 5

def f(x):
    x = 3
    print(x)

f(a)
print(a)

l = ["Hallo", "Welt"]

def f(x):
    x.append("!!!")
    print(x)
    
f(l)
print(l)

l = ["Hallo", "Welt"]

def f(x):
    x = ["Hallo", "Welt", "!!!"]
    print(x)
    
f(l)
print(l)

