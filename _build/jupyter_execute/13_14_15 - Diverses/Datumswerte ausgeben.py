## Datumswerte ausgeben

Wir haben uns jetzt schon angeschaut, wie wir Datumswerte vergleichen konnten.

In dieser Lektion lernst du:

- Wie du Datumswerte korrekt ausgeben kannst.

Dokumentation: https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior

from datetime import datetime

now = datetime.now()

print(now)

print(now.strftime("%d.%m.%Y"))
print(now.strftime("%Y-%m-%d"))
print(now.strftime("%Y%m%d"))

### Datumswerte einlesen

Das ganze funktioniert auch anders herum: Du kannst auch Datumswerte aus einem String extrahieren, wenn du z. B. mit den Python-Funktionen später mit dem Datum weiter rechnen willst.

d = "18.07.2017"

print(datetime.strptime(d, "%d.%m.%Y"))

