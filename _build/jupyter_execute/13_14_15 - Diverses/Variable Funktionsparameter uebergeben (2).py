## Variable Funktionsparameter übergeben

def f(**args):
    print(args)
    
f(key="value", key2="Value 2")

def g(key, param2):
    print(key)
    print(param2)
    
d = {"key": "Ich bin der Schlüssel", "param2": "Ich bin der Parameter"}

g(key=d["key"], param2=d["param2"])
g(**d)

%matplotlib inline
import matplotlib.pyplot as plt

def create_plot(**plot_params):
    print(plot_params)
    
    plt.plot([1, 2, 3], [5, 6, 5], **plot_params)
    plt.show()
    
create_plot(color="r", linewidth=10, linestyle="dashed")

