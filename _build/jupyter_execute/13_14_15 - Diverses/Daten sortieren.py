## Daten sortieren

Oft hast du das Problem, dass du z. B. eine Liste sortieren möchtest. Python stellt hier bereits eine fertige Funktion zur Verfügung, die wir aber korrekt ansteuern möchten.

In dieser Lektion lernst du, wie du:

- Daten in Python sortieren kannst
- Und die Sortierung beeinflussen kannst.

l = ["Max", "Monika", "Erik", "Franziska"]
l.sort()
print(l)

l = ["Max", "Monika", "Erik", "Franziska"]
l.sort(reverse=True)
print(l)

#### Eine eigene Funktion übergeben

def get_length(item):
    return len(item)
l = ["Max", "Monika", "Erik", "Franziska"]
l.sort(key=get_length)
print(l)

#### Aber... len(item) ist ja auch eine Funktion!

l = ["Max", "Monika", "Erik", "Franziska"]
l.sort(key=len)
print(l)

#### Die Daten sind nicht immer eine Liste!

d = {"Köln": "CGN", "Budapest": "BUD", "Saigon": "SGN"}

print(sorted(d, reverse = True))

l = ["Max", "Monika", "Erik", "Franziska"]
l.sort()
print(l)

l = ["Max", "Monika", "Erik", "Franziska"]
l2 = sorted(l)
print(l)
print(l2)

