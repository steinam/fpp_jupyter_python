## Daten sortieren

In dieser Lektion lernst du, was es mit Lambda-Funktionen auf sich hat. Diese sind unglaublich praktisch, weil sie eine kurze Schreibweise sind, wie du eine Funktion als Parameter übergeben kannst:

students = [
    ("Max", 3),
    ("Monika", 2),
    ("Erik", 3),
    ("Franziska", 1)
]

students.sort(key=lambda student: student[1])
print(students)

f = lambda student: student[1]
f(("Max", 1))

