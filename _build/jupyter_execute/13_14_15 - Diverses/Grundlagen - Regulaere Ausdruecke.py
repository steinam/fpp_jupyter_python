## Grundlagen: Regluäre Ausdrücke

Reguläre Ausdrücke erlauben dir, Strings noch flexibler zu durchsuchen. Beispielsweise kannst du mit einem regulären Ausdruck alle Zahlen in einem String finden oder validieren, dass eine E-Mail-Adresse grundsätzlich existieren könnte.

Aber wie funkionieren die?

import re

sentence = "Ich habe 30 Hunde, die jeweils 4 Liter Wasser brauchen und 2 kg Nahrung."
re.findall("[0-9]+", sentence)

sentence = "Ich habe 30 Hunde, die jeweils 4 Liter Wasser brauchen und 2 kg Nahrung."
re.search("[0-9]+", sentence)

re.search("der?", "Hallo der Hallo")

print(re.search("der*", "Hallo de Hallo"))
print(re.search("der*", "Hallo der Hallo"))
print(re.search("der*", "Hallo derrrrrrrr Hallo"))

print(re.search("der+", "Hallo de Hallo"))
print(re.search("der+", "Hallo der Hallo"))
print(re.search("der+", "Hallo derrrrrrrr Hallo"))

print(re.search("[0123456789]+", "Hallo 123 Hallo"))
print(re.search("[0-9]+", "Hallo 123 Hallo"))

print(re.findall("[0-9]+", "Hallo 123 Hallo 321"))

