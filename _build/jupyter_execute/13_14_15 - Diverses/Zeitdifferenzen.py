## Zeitdifferenzen: Rechnen mit Datumswerten

In dieser Lektion lernst du:

- Wie du in Python mit Datumswerten rechnen kannst,
- Und was es mit einem "timedelta" auf sich hat.

from datetime import datetime, timedelta

now = datetime.now()

print(now)
print(now + timedelta(days = 20, hours = 4, minutes = 3, seconds = 1))

day = datetime(2017, 8, 20)

td = day - now

print(td)

print(datetime(2018, 1, 1) + td)

