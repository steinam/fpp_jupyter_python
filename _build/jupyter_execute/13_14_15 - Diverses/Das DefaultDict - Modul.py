## Das DefaultDict-Modul

Das DefaultDict-Modul stellt eine ganz besondere Funktionalität zur Verfügung: Du kannst damit ein Dictionary erstellen, welches sich mehr oder weniger automatisch mit initialen Werten befüllt!

Das möchten wir uns natürlich mal anschauen:

from collections import defaultdict

def generate():
    print("generate() wurde aufgerufen!")
    return 0

d = defaultdict(generate)

d["existiertNicht"] = d["existiertNicht"] + 5
print(d)

p = defaultdict(int)
words = ["Hallo", "Hallo", "Welt"]

for word in words:
    p[word] = p[word] + 1

print(p)

