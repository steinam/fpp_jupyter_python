## String - Methoden

In diesem Dokument möchte ich dir einen kurzen Überblick über wichtige String-Methoden geben.

Mit der `.upper()`- bzw. `.lower()`-Methode kannst du dafür sorgen, dass alle Zeichen in Groß- bzw. Kleinbuchstaben angezeigt werden:

w = "Hallo"
print(w.upper())
print("Hallo".upper())

w = "Hallo"
print(w.lower())
print("Hallo".lower())

Mit der `.startswith()` bzw. `.endswith()`-Methode kannst du prüfen, ob ein String mit einem anderen String beginnt / aufhört:

sentence = "Ist das Wetter heute gut???"

if sentence.endswith("???"):
    print("Der Satz endet mit drei Fragezeichen")
    
if sentence.startswith("Ist"):
    print("Der Satz beginnt mit einem 'ist'")

### Die `.strip()`-Methode

Standardmäßig entfernt die `.strip()`-Methode Leerzeichen vom Anfang und vom Ende des Strings:

"   Hallo Welt.    ".strip()

Du kannst der `strip()`-Methode aber auch als Parameter übergeben, welche Zeichen entfernt werden sollen. Hier in den nächsten Beispielen sagen wir z. B., dass nur Unterstrichte und Punkte entfernt werden sollen.

Die `.lstrip()`- bzw. `.rstrip()`-Methode funktionieren analog der `.strip()`-Methode, wobei aber `.lstrip()` nur die linke Seite und `.rstrip()` nur die rechte Seite betrachtet:

word = "____Hallo.__"
print(word.strip("_."))
print(word.lstrip("_"))
print(word.rstrip("_"))

sentence = "Ist das Wetter heute, und morgen gut???"
print(sentence.rstrip("!?.,"))

### Die `.find()`-Methode

Mit der `.find()`-Methode kannst du herausfinden, an welcher Stelle ein Zeichen in einem String vorkommt. Beispielsweise können wir so herausfinden, dass das Komma an der 21. Stelle (Position 20, also) vorkommt.

Wenn die `.find()`-Methode die Zahl `-1` zurückgibt, bedeutet dass, dass das Zeichen im String nicht vorkommt:

sentence = "Ist das Wetter heute, und morgen gut???"
print(sentence.find(","))
print(sentence.find("!"))

### Zeichen ersetzen (`.replace()`)

Mit der `.replace()`-Methode kannst du eine Ersetzung durchführen. Beispielsweise kannst du so das Komma durch ein Semikolon ersetzen lassen, etc.:

sentence = "Ist das Wetter heute, und morgen gut???"

print(sentence.replace(",", ";"))
print(sentence.replace("u", "ü"))
print(sentence.replace("und", "oder"))

