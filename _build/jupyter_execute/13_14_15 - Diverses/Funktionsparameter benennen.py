## Funktionsparameter benennen

Manchmal arbeitest du mit einer Funktion, die sehr viele Parameter übergeben bekommt. Dann wird der Funktionsaufruf sehr schnell sehr unübersichtlich...

In dieser Lektion lernst du:

- Was Standardparameter sind,
- Und wie du benannte Parameter einer Funktion übergeben kannst.

def multi_print(number = 3, word = "Hallo"):
    for i in range(0, number):
        print(word)
        
multi_print(5)
multi_print(word = "Welt", number = 5)

