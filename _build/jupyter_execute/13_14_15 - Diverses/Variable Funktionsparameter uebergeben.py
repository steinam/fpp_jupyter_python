## Variable Funktionsparameter übergeben

def f(a, b, c):
    print(a)
    print(b)
    print(c)
    
l = [1, 2, 3]
f(l[0], l[1], l[2])

f(*l)

def calculate_max(*params):
    print(params)
    current_max = params[0]
    for item in params:
        if item > current_max:
            current_max = item
    return current_max
    
calculate_max(1, 2, 3)

def calculate_max(current_max, *params):
    print(current_max)
    print(params)
    for item in params:
        if item > current_max:
            current_max = item
    return current_max
    
calculate_max(1, 2, 3)

