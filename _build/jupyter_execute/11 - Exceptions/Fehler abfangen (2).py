## Exceptions in Python

In dieser Lektion lernst du, wie du:

- mehrere Exceptions abfangen kannst
- eigene Exceptions erstellen kannst.

try:
    with open("datei.xyz", "r") as file:
        print(file)
    print(5 / 0)
except ZeroDivisionError:
    print("Du darfst nicht durch 0 teilen")
except FileNotFoundError:
    print("FileNotFoundError ist aufgetreten")

def do_something():
    print(5 / 0)
    
try:
    do_something()
except ZeroDivisionError:
    print("Du darfst nicht durch 0 teilen")

class InvalidEmailError(Exception):
    pass

def send_mail(email, subject, content):
    if not "@" in email:
        raise InvalidEmailError("email does not contain an @")
try:     
    send_mail("hallo", "Betreff", "Inhalt")
except InvalidEmailError:
    print("Bitte gebe eine gültige E-Mail ein")

