## Exceptions in Python

In dieser Lektion lernst du:

- Wie du damit umgehst, wenn während der Laufzeit deines Programms ein Fehler auftritt.

x = 0


print(5 / x)

with open("datei.xyz", "r") as file:
    print(file)

try:
    print(5 / 0)
    print(4)
except ZeroDivisionError:
    print("Durch null teilen ist nicht erlaubt!")
print(5)

try:
    with open("datei.xyz", "r") as file:
        print(file)
except FileNotFoundError:
    print("Die Datei konnte nicht geöffnet werden")

## Exceptions in Python

In dieser Lektion lernst du:

- Wie du mehrere Exceptions abfangen kannst
- Und wie du eigene Exceptions erstellen kannst

try:
    with open("datei.xyz", "r") as file:
        print(file)
    print(5 / 0)
except ZeroDivisionError:
    print("Du darfst nicht durch 0 teilen")
except FileNotFoundError:
    print("FileNotFoundError ist aufgetreten")

def do_something():
    print(5 / 0)
    
try:
    do_something()
except ZeroDivisionError:
    print("Du darfst nicht durch 0 teilen")

class InvalidEmailError(Exception):
    pass

def send_mail(email, subject, content):
    if not "@" in email:
        raise InvalidEmailError("email does not contain an @")
try:     
    send_mail("hallo", "Betreff", "Inhalt")
except InvalidEmailError:
    print("Bitte gebe eine gültige E-Mail ein")

## Exceptions in Python

In dieser Lektion lernst du:

- Wie du mehrere Exceptions abfangen kannst
- Und wie du eigene Exceptions erstellen kannst

try:
    with open("datei.xyz", "r") as file:
        print(file)
    print(5 / 0)
except ZeroDivisionError:
    print("Du darfst nicht durch 0 teilen")
except FileNotFoundError:
    print("FileNotFoundError ist aufgetreten")

def do_something():
    print(5 / 0)
    
try:
    do_something()
except ZeroDivisionError:
    print("Du darfst nicht durch 0 teilen")

class InvalidEmailError(Exception):
    pass

def send_mail(email, subject, content):
    if not "@" in email:
        raise InvalidEmailError("email does not contain an @")
try:     
    send_mail("hallo", "Betreff", "Inhalt")
except InvalidEmailError:
    print("Bitte gebe eine gültige E-Mail ein")