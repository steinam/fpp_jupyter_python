## Exceptions in Python

In dieser Lektion lernst du, wie du:

- nach einem Fehler sauber "aufräumen" kannst,
- dies mit einem "finally" korrekt erzwingen kannst.

try:
    file = open("existiert.txt", "r")
    print(file)
    print(5 / 0)
except FileNotFoundError:
    print("Datei wurde nicht gefunden")
finally:
    print("FINALLY!!!")
    file.close()

