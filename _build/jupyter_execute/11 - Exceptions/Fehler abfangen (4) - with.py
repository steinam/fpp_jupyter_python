## Exceptions in Python

In dieser Lektion lernst du:

- Was genau es mit dem "with"-Konstrukt auf sich hat, welches du in diesem Kurs schon benutzt hast.

try:
    file = open("existiert.txt", "r")
    print(file)
    print(5 / 0)
except FileNotFoundError:
    print("Datei wurde nicht gefunden")
finally:
    print("FINALLY!!!")
    file.close()
    


def do_something():
    with open("existiert.txt", "r") as file:
        print(file)
        return "HALLO"
    
do_something()

