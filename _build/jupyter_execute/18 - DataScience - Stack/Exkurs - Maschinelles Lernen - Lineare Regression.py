## Exkurs: Maschinelles Lernen (Lineare Regression)

In dieser Lektion machen wir einen kleinen Exkurs: Wir beschäftigen uns ein bisschen mit maschinellem Lernen. Aber keine Sorge, geht ganz easy :)

In dieser Lektion lernst du:

- Wie du eine Linie durch die Geburtsstatstik zeichnest, die den Trend der Statistik veranschaulicht

Indem wir diese Linie verlängern, können wir versuchen Zukunftsprognosen treffen.

Wir schauen uns nochmal die Häufigkeit des Namens "Anna" für Frauen in Kalifornien an:

# pandas zum Einlesen der csv-Datei und matplotlib zur grafischen Darstellung einbinden
%matplotlib inline
import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv("../data/names.csv")
df.head()

Wir filtern das DataFrame und plotten das Resultat:

name = "Anna"
gender = "F"
state = "CA"

df2 = df[df["Name"] == name]
df3 = df2[df2["Gender"] == gender]
df4 = df3[df3["State"] == state]

df5 = df4.sort_values("Year")

xs = df5["Year"]
ys = df5["Count"]

plt.plot(xs, ys)
plt.show()

### Maschinelles Lernen mit dem `scikit-learn`-Modul

Die `LinearRegression()` - Funktion, die eine "passende" Gerade durch unseren Grapfen legt, importieren wir aus dem `scikit-learn` - Modul: 

from sklearn.linear_model import LinearRegression

Bevor wir die Funktion anwenden, müssen wir die Parameter vorbereiten: unsere Eingabewerte (die Jahreszahlen) müssen als höherdimensionale Arrays vorliegen, d.h. wir "verpacken" sie:

xsl = []

for x in xs:
    xsl.append([x])
    
print(xsl)

Jetzt können wir mit `LinearRegression()` ein Modell mit den vorliegenden Daten trainieren. Bei dem "Modell" handelt es sich hier nur um eine Gerade.

model = LinearRegression()

model.fit(xsl, ys)

Welche Voraussagen macht das Modell für bestimmte Jahre (die wir vergleichen könnten, weil dazu ja auch die echten Daten vorliegen)?

model.predict([[1920], [1950]])

Wir schauen uns das Modell über die gesamte Vergangenheit an:

model.predict(xsl)

Anschaulicher sieht unser Modell (die Gerade) so aus:

predicted = model.predict(xsl)

plt.plot(xs, ys)
plt.plot(xs, predicted)
plt.show()

Schauen wir uns zum Schluss doch noch eine Zukunftsprognose an, d.h. welche Häufigkeit für den weiblichen Vornamen "Anna" in Kalifornien liefert die in die Zukunft verlängerte Linie?

model.predict([[2050]])