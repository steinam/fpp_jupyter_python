## Aufgabe: Datei einlesen und visualisieren

In den ersten paar Abschnitten des Kurses haben wir eine CSV-Datei eingelesen, und die Daten dann als Grafik angezeigt. In dieser Lektion möchte ich dir die Aufgabe geben, diese CSV-Datei erneut einzulesen und die Daten zu visualisieren - aber diesmal mit Pandas!

Aufgabe: Lade die ../data/names.csv - Datei, und zeichne eine Grafik für den Namen "Anna", das Geschlecht "F" (female) und den Staat "CA". Auf der X-Achse sollen die Jahre abgetragen werden, auf der Y-Achse, wie oft der Name vergeben wurde.

name = "Anna"
gender = "F"
state = "CA"

import pandas as pd

df = pd.read_csv("../data/names.csv")
df.head()

df2 = df[df["Name"] == name]
df3 = df2[df2["Gender"] == gender]
df4 = df3[df3["State"] == state]

df5 = df4.sort_values("Year")

%matplotlib inline
import matplotlib.pyplot as plt

plt.plot(df5["Year"], df5["Count"])
plt.show()

