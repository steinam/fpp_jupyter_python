## Pandas + Excel + Matplotlib

In dieser Lektion lernst du:

- Wie du Daten aus einer Excel - Datei einliest
- Und diese dann mit Matplotlib direkt plotten kannst

### Mit Pandas eine Excel - Datei einlesen

Entsprechend zur `read_csv()` - Funktion stellt `pandas` auch die Funktion **`read_exel()`** zur Verfügung:

import pandas as pd

df = pd.read_excel("daten.xlsx")
df.head()

Näheres zur `read_excel()` - Funktion findest du in der offizielen Funktion von `pandas` hier: https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_excel.html

Mit dem DataFrame gehst du dann wie gewohnt um:

year = df["Jahr"]

sales = df["Umsatz"]

print(sales)

print(year)

Diese Daten (den Umsatz pro Jahr) wollen wir jetzt grafisch in einem Plot darstellen. :-)

### Ein DataFrame plotten

Zuerst müssen wir wiedermal `matplotlib` einbinden:

%matplotlib inline
import matplotlib.pyplot as plt

Die Daten aus den Spalten unseres DataFrames können wir dann der `plot()` - Funktion von matplotlib übergeben.

plt.plot(year, sales)
plt.show()