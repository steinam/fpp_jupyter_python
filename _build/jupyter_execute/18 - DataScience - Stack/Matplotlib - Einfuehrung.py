## Matplotlib: Einführung

Du weisst bereits, wie du csv-Dateien und Excel-Dateien mit `pandas` einlesen und dann filtern und sortieren kannst und mit `Numpy` numerische Berechnungen auf Datenmengen ausführen kannst. 

Jetzt schauen wir uns genauer an, wie wir die Daten dann mit `matplotlib` schön darstellen können.

# mit diesem Steuerzeichen legen wir fest, dass Grafiken direkt im Notebook angezeigt werden
%matplotlib inline

import matplotlib.pyplot as plt

plt.plot([1, 2, 3], [5, 4, 3])
plt.show()

Alternativ können wir auch interaktive Grafiken anzeigen lassen.

Unter Umständen musst du die nächste Zelle mehrmals ausführen.

%matplotlib notebook
import matplotlib.pyplot as plt

plt.plot([1, 2, 3], [5, 4, 3])

Wir können Grafiken auch in einem neuen Fenster anzeigen lassen.

Vor der Ausführung der nachfolgenden Zelle musst du in der Leiste oben auf **Kernel** und dann auf **Restart** gehen.

%matplotlib tk
import matplotlib.pyplot as plt

plt.plot([1, 2, 3], [5, 4, 3])