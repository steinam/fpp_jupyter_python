## Pandas: Daten filtern, Schleifen, ...

In dieser Lektion lernst du: 

- Wie du das DataFrame mit einer for - Schleife betrachten kannst
- Wie du nach bestimmten Werten filtern kannst

Zuerst laden wir wieder das pandas-Modul und dann die csv-Datei als DataFrame:

import pandas as pd
df = pd.read_csv("../data/astronauts.csv")

df.head()

### Mit einer Schleife durch ein DataFrame gehen 

Mit der `iterrows()`- Methode wird ein DataFrame in eine Form umgewandelt, die es erlaubt, leicht mit einer `for`-Schleife dadurch zugehen.

for row in df.iterrows():
    print(row)

Genauer werden quasi mit `iterrows()` für jeden Eintrag zweielementige Tupel gebildet, die aus dem Index des Eintrags und den Daten des Eintrags bestehen.

for pos, d in df.iterrows():
    print(pos)

for pos, d in df.iterrows():
    print(d["Name"])

### Daten mit Vergleichen filtern

Daten zu filtern kennst du schon von `Numpy`. :-) Da `pandas` auf `Numpy` aufbaut, gehst du mit den `DataFrames` von `pandas` genauso vor wie mit den `Arrays` von `Numpy`.

df["Year"] < 1990

df[df["Year"] < 1990]

Beachte, dass dabei nur eine Kopie des `DataFrames` gefiltert wird! `df` bleibt bei solchen Operationen unverändert.

### Mehrfache Filter
Natürlich kannst du auch mehrere Filter an ein `DataFrame` anlegen. Achte darauf, wenn du in mehrfachen Stufen filtern möchtest, gefilterte `DataFrames` in neuen Variablen zwischenzuspeichern:

df2 = df[df["Year"] < 1990]
df3 = df2[df2["Gender"] == "Male"]
df3