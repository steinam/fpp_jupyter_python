## Numpy: Arrays filtern

In dieser Lektion lernst du:

- Wie du `Numpy` - Arrays filtern kannst

Zuerst laden wir natürlich wieder das `Numpy`-Modul:

import numpy as np

### Ein Array mit einem anderen Array filtern

Wir können ein Array mit Boolean - Einträgen dazu verwenden, ein anderes Array zu filtern.

a = np.array([1, 2, 3, 4])
print(a)

Ein Array braucht nicht notwendigerweise nur aus Zahlen bestehen:

b = np.array([False, True, True, False])
print(b)

Mithilfe der `[ ]-Schreibweise`, die du schon vom _Zugriff auf Listen_ und vom _list slicing_ her kennst, können wir ein Numpy-Array einem anderen Numpy-Array übergeben, das dann gefiltert wird:

a[b]

Beim zu filternden Array bleiben also nur die Werte stehen, an deren Index im Filter-Array ein True-Boolean steht.

### Mit Vergleichen filtern

Wir können spezifischer filtern, indem wir Vergleiche für Arrays formulieren. Wie die Rechenoperationen werden auch die Vergleichsoperationen auf jeden Wert eines Arrays angewendet - jeder Wert wird einzeln verglichen:

c = a >= 3
print(c)

Einen solchen Array, den wir mithilfe einer Vergleichsoperation erzeugt haben, können wir dann als Filter-Array verwenden:

a[c]

Oder kurz:

a[a >= 3]

Somit steht uns eine kompakte Schreibweise für das Filtern von Arrays zur Verfügung. :-)