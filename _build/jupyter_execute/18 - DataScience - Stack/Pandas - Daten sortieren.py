## Pandas: Daten sortieren

Oft möchtest du die Daten noch in einer Spalte sortieren lassen. Das zeige ich dir hier in dieser Lektion!

import pandas as pd
df = pd.read_csv("../data/astronauts.csv")

df.head()

### Daten sortieren mit `sort_values()`

Mit **`sort_values()`** kannst du das DataFrame nach den Einträgen in einer bestimmten Spalte, die du als Parameter übergibst, sortieren.

df2 = df.sort_values("Name")
df2

Standardmäßig wird dann aufsteigend sortiert. Mit dem `ascending` - Attribut kannst du dieses Verhalten auch umstellen:

df2 = df.sort_values("Name", ascending = False)
df2

Wenn du z.B. alle Namen in absteigender Reihenfolge durchgehen möchtest, empfiehlt es sich erst das ganze DataFrame zu sortieren und dann mit den sortierten Namen zu arbeiten.

for name in df2["Name"]:
    print(name)