## Numpy: Mehrdimensionale Arrays

In Numpy kannst du auch mit **mehrdimensionalen Arrays** (**Matrizen**) arbeiten.

import numpy as np

Eindimensionale Arrays kennst du schon:

a = np.array([1, 2, 3, 4, 5, 6, 7, 8])
print(a)

Arrays können aber auch weitere Arrays enthalten, dann sprechen wir von **mehrdimensionalen Arrays**:

a = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
print(a)

Bei einem zweidimensionalen Array liegt ein Array in einem Array vor, bei einem dreidimensionalen Array schon ein Array in einem Array in einem Array. Doch das klingt komplizierter als es ist. Du brauchst einfach die `[` vor dem ersten Element (oder nach dem letzten Element) des Arrays zu zählen, um zu sehen, wie viele Dimensionen/Ebenen das Array besitzt.

### Die Form eines Arrays mit `reshape()` verändern
Mit der **`reshape()`**-Methode kannst du einen Array restrukturieren, sodass innerhalb des Arrays Unter-Arrays gebildet werden und die Werte auf diese neuen Unter-Arrays aufgeteilt werden. Dazu übergibst du `reshape()` als Parameter ein Tupel mit Zahlen, um die Maße für die neue Gestalt des Arrays festzulegen. 

Jede der Zahlen in dem Tupel steht dafür, wie viele Elemente es in einer Dimension geben soll.

a.reshape((2, 4))

Mit dem Parameter `(2, 4)` sagen wir also, dass der äußere Array zwei Elemente enthalten soll (d.h. zwei Unter-Arrays), und die Arrays der zweiten Stufe jeweils vier Elemente enthalten sollen (d.h. die Werte).

Bei zwei Dimensionen, d.h. einem Tupel mit zwei Elementen als Parameter, kannst du dir den ersten Wert des Tupels als Anzahl der Zeilen und den zweiten Wert als die Anzahl der Spalten vorstellen.

Auf die Elemente in einem solchen mehrdimensionalen Array kannst du mit mehrfacher Verwendung der `[ ]-Schreibweise` zugreifen.

reshaped = a.reshape((2, 4))

print(reshaped[0])
print(reshaped[0][0])
print(reshaped[1])
print(reshaped[1][3])

Natürlich kannst du mit `reshape()` nur Verschachtelungen erzeugen, die zur Gesamtzahl der Einträge passen, d.h. das Produkt über alle Werte aus dem Tupel, das du als Parameter übergibst, und die Anzahl der Elemente aus dem Array müssen übereinstimmen.

Sonst passiert das:

c = np.array([1, 2, 3, 4, 5, 6, 7])
c.reshape((4, 2))

### Parameter-Platzhalter: Anzahl von Einträgen in einer Dimension offen lassen

Du kannst auch **-1** als Platzhalter für die Anzahl der Einträge in einer Ebene benutzen, statt sie zu spezifizieren; Numpy füllt dann die neuen Unter-Arrays automatisch mit Einträgen auf.

a.reshape((-1, 4))

a.reshape((4, -1))

So können wir schnell ein eindimensionales Array erzeugen:

b = np.array([[1, 2, 3], [4, 5, 6]])
b.reshape(-1)

### Die Dimensionen eines Arrays abfragen
Die Maße eines Arrays können wir mit der `shape`-Eigenschaft des Array-Objektes abfragen.

b.shape