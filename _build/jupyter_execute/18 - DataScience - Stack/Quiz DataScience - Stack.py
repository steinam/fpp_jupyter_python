## Quiz: Der DataScience - Stack

Viele Probleme lassen sich durch die Verwendung der richtigen Tools sehr viel einfacher lösen. Daher gibt's in dieser Lektion ein kleines Quiz, wo wir uns genau das nochmal anschauen. :-)

#### Aufgabe 1: Wann welches Tool?

Du möchtest eine Grafik zeichnen. Welches Tool verwendest du dazu?

- A) Pandas
- B) Numpy
- C) Matplotlib

Richtige Antwort: C

#### Aufgabe 2: Wann welches Tool?

Du möchtest eine CSV-Datei einlesen und die Daten komfortabel verarbeiten. Welches Tool verwendest du dazu?

- A) Pandas
- B) Numpy
- C) Matplotlib

Richtige Antwort: A

#### Aufgabe 3: Wann welches Tool?

Du hast eine Python-Liste und möchtest für jeden Eintrag ein paar mathematische Berechnungen ausführen. Du möchtest beispielsweise jeden Eintrag mit 2 multiplizieren und anschließend das Quadrat bilden. 

Welches Tool unterstützt dich hierbei perfekt?

- A) Pandas
- B) Numpy
- C) Matplotlib

Richtige Antwort: B

