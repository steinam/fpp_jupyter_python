## Aufgabe: Datei einlesen und visualisieren

In den ersten paar Abschnitten des Kurses haben wir eine CSV-Datei eingelesen, und die Daten dann als Grafik angezeigt. In dieser Lektion möchte ich dir die Aufgabe geben, diese CSV-Datei erneut einzulesen und die Daten zu visualisieren - aber diesmal mit Pandas!

Aufgabe: Lade die ../data/names.csv - Datei, und zeichne eine Grafik für den Namen "Anna", das Geschlecht "F" (female) und den Staat "CA". Auf der X-Achse sollen die Jahre abgetragen werden, auf der Y-Achse, wie oft der Name vergeben wurde.

name = "Anna"
gender = "F"
state = "CA"