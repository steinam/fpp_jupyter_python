## Matplotlib unterstützt auch noch weitere Diagrammtypen...

In dieser Lektion lernst du wie du mit `matplotlib` folgende Diagrammtypen zeichnen kannst:

- Kreisdiagramme
- Balkendiagramme
- Punktediagramme

%matplotlib inline
import matplotlib.pyplot as plt

### Kreisdiagramm

Für ein `Kreisdiagramm` rufst du die `pie()`-Funktion auf und übergibst ihr als Parameter eine Liste. Die Einträge in der Liste stehen für die Größe der "Kuchenstücke" (im Verhältnis zur Summe aller Listenelemente):

plt.pie([1, 2, 3])
plt.show()

### Balkendiagramm

`Balkendiagramme` erzeugst du mit der `bar()`-Methode. Ihr musst du als Parameter zwei Listen übergeben: eine für die Werte auf der x-Achse (wo Balken sein sollen), eine für die Werte auf der y-Achse (also die Höhe der Balken). Dementsprechend müssen die beiden Listen gleich viele Elemente beinhalten:

plt.bar([1, 2, 4], [5, 6, 5])
plt.show

### Punktediagramm
Mit der `scatter()`-Funktion kannst du dir auch einfach nur die Punkte anzeigen lassen:

plt.scatter([1, 2, 4], [5, 6, 5])
plt.show()

Alle diese Diagramme kannst du so modifizieren, wie du es in einer früheren Lektion gesehen hast:

plt.scatter([1, 2, 4], [5, 6, 5], color = "#ff0000", marker = "x")
plt.show()