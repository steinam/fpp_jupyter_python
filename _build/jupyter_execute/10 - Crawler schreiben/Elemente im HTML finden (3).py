## Elemente im HTML finden

In dieser Lektion lernst du:

- Wie du aus einer .html-Datei Informationen extrahieren kannst

Dazu zerlegen wir mit BeautifulSoup (https://www.crummy.com/software/BeautifulSoup/bs4/doc/) die .html-Datei!

import requests
from bs4 import BeautifulSoup

class CrawledArticle():
    def __init__(self, title, emoji, content, image):
        self.title = title
        self.emoji = emoji
        self.content = content
        self.image = image
        
class ArticleFetcher():
    def fetch(self):
        r = requests.get("http://python.beispiel.programmierenlernen.io/index.php")
        doc = BeautifulSoup(r.text, "html.parser")
        
        articles = []
        for card in doc.select(".card"):
            emoji = card.select_one(".emoji").text
            content = card.select_one(".card-text").text
            title = card.select(".card-title span")[1].text
            image = card.select_one("img").attrs["src"]

            crawled = CrawledArticle(title, emoji, content, image)
            articles.append(crawled)
        return articles

fetcher = ArticleFetcher()
fetcher.fetch()

