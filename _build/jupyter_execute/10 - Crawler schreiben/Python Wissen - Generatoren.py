## Python Wissen: Generatoren

In dieser Lektion geht's um ein weiteres Python-Konzept, welches unseren Crawler etwas vereinfachen kann: Generatoren. 

Problem: Manchmal interessieren dich nur die ersten 5 Einträge, manchmal möchtest du alle Einträge einlesen. Wie bekommen wir es hin, dass die .fetch()-Methode automatisch erkennt, wie viele Einträge mich interessieren?

In dieser Lektion lernst du:

- Was Generatoren sind
- Und wie du diese in Python verwenden kannst.

def gen_list():
    liste = []
    for i in range(0, 10):
        print("liste: " + str(i))
        liste.append(i)
    return liste

for element in gen_list():
    print("for: " + str(element))

def gen_generator():
    for i in range(0, 10):
        print("gen: " + str(i))
        yield i

for element in gen_generator():
    print("for: " + str(element))

for element in gen_generator():
    if element == 4:
        break
    print("for: " + str(element))

