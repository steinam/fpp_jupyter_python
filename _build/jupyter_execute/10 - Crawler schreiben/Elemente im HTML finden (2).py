## Elemente im HTML finden

In dieser Lektion lernst du:

- Wie du aus einer .html-Datei Informationen extrahieren kannst.

Dazu zerlegen wir mit BeautifulSoup (https://www.crummy.com/software/BeautifulSoup/bs4/doc/) die .html-Datei!

# Zuerst holen wir uns unsere Seite, die wir eigentlich einlesen wollten...

import requests
r = requests.get("http://python.beispiel.programmierenlernen.io/index.php")

from bs4 import BeautifulSoup

doc = BeautifulSoup(r.text, "html.parser")

for card in doc.select(".card"):
    emoji = card.select_one(".emoji").text
    content = card.select_one(".card-text").text
    title = card.select(".card-title span")[1].text
    image = card.select_one("img").attrs["src"]
    
    print(image)
    print(emoji)
    print(content)
    print(title)
        

