## Elemente im HTML finden

In dieser Lektion lernst du:

- Wie du aus einer .html-Datei Informationen extrahieren kannst

Dazu zerlegen wir mit BeautifulSoup (https://www.crummy.com/software/BeautifulSoup/bs4/doc/) die .html-Datei!

# Zuerst holen wir uns unsere Seite, die wir eigentlich einlesen wollten...

import requests
r = requests.get("http://python.beispiel.programmierenlernen.io/index.php")

from bs4 import BeautifulSoup

html = """
    <html>
        <body>
            <p class="something">Ich bin ein Absatz!</p>
            <p>Ich bin noch ein Absatz</p>
        </body>
    </html>
"""

doc = BeautifulSoup(html, "html.parser")

for p in doc.find_all("p"):
    print(p.attrs)
    print(p.text)

