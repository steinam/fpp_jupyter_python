## Das Requests - Modul

In dieser Lektion lernst du:

- Wie du eine Webseite herunterlädst, und dir den HTML-Code davon anzeigen kannst.

Dazu verwendne wir das requests-Modul (http://docs.python-requests.org/en/master/).

Wir werden folgende Seite crawlen: http://python.beispiel.programmierenlernen.io/index.php.

import requests

r = requests.get("http://python.beispiel.programmierenlernen.io/index.php")

print(r.status_code)
print(r.headers)
print(r.text)

