## Module in Python

In Python hast du die Möglichkeit, Code in Module zu packen, die du dann in verschiedenen Stellen verwenden kannst.

Die Datei `hallo.py` besteht hierbei aus folgendem Inhalt:

```python
# hallo.py

def welt():
    print("Hallo Welt")
    
def mars():
    print("Hallo Mars")
```

Wie können wir diese Datei jetzt einbinden?

import hallo

hallo.welt()
hallo.mars()

from hallo import welt, mars

welt()
mars()

from hallo import *

welt()
mars()