## Module und Ordner

In Python hast du auch die Möglichkeit, Module mit Hilfe eines Ordners zu definieren. 

In dieser Lektion lernst du: 

- Wie du Module mit Hilfe eines Ordners definierst, und so dein Modul besser strukturieren kannst
- Was es mit der \_\_init\_\_.py auf sich hat

from hallom import datei

datei.f()

from hallom import *
datei.f()

import hallom
hallom.datei.f()

