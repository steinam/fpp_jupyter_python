## Module in Python

In Python hast du die Möglichkeit, Code in Module zu packen, die du dann in verschiedenen Stellen verwenden kannst.

In dieser Lektion:

- erstellst du ein kleines Modul
- und importierst dieses Modul in dieses Notebook.

import hallo

hallo.welt()
hallo.mars()

from hallo import welt, mars

welt()
mars()

from hallo import *

welt()
mars()



