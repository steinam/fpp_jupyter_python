# Vorstellung weiteres Modul: CSV

In dieser Lektion lernst du, wie du:

- mit dem Python Module - Index umgehst (https://docs.python.org/3/py-modindex.html)
- mit Moduls noch komfortabler .csv-Dateien einlesen kannst am Beispiel des CSV. 

import sys
print(sys.version)

import csv
with open("datei.csv", newline='') as file:
    csv_file = csv.reader(file, delimiter=",")
    for line in csv_file:
        print(line)

import csv
with open("fromexcel.csv", newline='') as file:
    csv_file = csv.reader(file, delimiter=";", quotechar='"')
    for line in csv_file:
        print(line)

