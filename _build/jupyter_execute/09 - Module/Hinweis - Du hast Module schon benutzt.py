## In dieser Lektion lernst du, wie du Module schon benutzt hast!

%matplotlib inline

import matplotlib.pyplot

matplotlib.pyplot.plot([1, 2, 3], [5, 4, 5])
matplotlib.pyplot.show()

from matplotlib import pyplot

pyplot.plot([1, 2, 3], [5, 4, 5])
pyplot.show()

%matplotlib inline

import matplotlib.pyplot as plt

plt.plot([1, 2, 3], [5, 4, 5])
plt.show()

