## Quiz - Module

#### Aufgabe 1

Wie bindest du das Modul `pandas` ein?

- A) `import pandas`
- B) `pandas.import()`
- C) `import pandas from zoo`
- D) `__import(pandas)__`

Richtige Lösung: 

#### Aufgabe 2

Wie rufst du die Funktion `read_csv()`aus dem Modul `pandas` auf, nachdem du das ganze Modul mit dem `import`-Befehl eingebunden hast?

- A) `read_csv(pandas)`
- B) `pandas.pandas.read_csv()`
- C) `pandas: pandas.read_csv()`
- D) `pandas.read_csv()`

Richtige Lösung: 

#### Aufgabe 3

Mit welchem Befehl musst du das Modul `pandas` einbinden, um die Funktionen `read_csv()` und `read_excel()` aus dem Modul direkt verwenden zu können (also, wie jede in Standard-Python bereits vorhandene Funktion aufrufen kannst)?

- A) `from pandas import read_csv(), read_excel()`
- B) `from pandas import read_csv, read_excel`
- C) `import read_csv(), read_excel() from pandas`
- D) `import pandas.read_csv, import pandas.read_excel`

Richtige Lösung: 

#### Aufgabe 4

Wie kannst du das Modul `pandas` einbinden und dabei den Zugriffsnamen umbenennen, so dass du in der Folge eine Abkürzung (hier: `pd`) anstelle des Modulnamensbenutzen kannst?

- A) `import pandas(pd)`
- B) `import pandas, pandas = pd`
- C) `import pandas as pd`
- D) `import pandas.pd`

Richtige Lösung: 

