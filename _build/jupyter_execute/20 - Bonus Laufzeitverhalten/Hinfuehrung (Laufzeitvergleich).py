# Hinführung: O-Notation

Schauen wir uns mal ein paar Beispiele an, wie sich die Anzahl der Aufrufe des print()-Befehles in Abhängigkeit zu `n` verhalten könnte:

### Konstante Laufzeit

def f(n):
    print("--- nächstes n: " + str(n) + "---")
    for i in range(1, 6): 
        print(str(i) + ". Durchlauf von 5")

for n in range(1, 6):
    f(n)

### Lineare Laufzeit

# wir schauen uns an, wie sich die Ausgabe der Funktion f in Abhängigkei vom Parameter n verändert

def f(n):
    print("--- nächstes n: " + str(n) + "---")

    for i in range(1, n + 1): 
        print(str(i) + ". Durchlauf von " + str(n))

for n in range(1,6):
    f(n)

### Quadratische Laufzeit

def f(n):
    print("--- nächstes n: " + str(n) + "---")
    for j in range(1, n + 1):
        for i in range(1, n + 1):
            print("(i = " + str(i) + ", j = " + str(j) + ") - Durchlauf, n = " + str(n))

for n in range(1, 6):
    f(n)

### Vergleich plotten

import matplotlib.pyplot as plt
import numpy as np

%matplotlib inline

plt.plot(np.arange(1, 6), np.repeat(5, 5), label="Konstante Laufzeit")
plt.plot(np.arange(1, 6), np.arange(1, 6), label="Lineare Laufzeit")
plt.plot(np.arange(1, 6), np.arange(1, 6) ** 2, label="Quadratrische Laufzeit")
plt.legend()

plt.xlabel("n")
plt.ylabel("Anzahl print() - Befehle")
plt.show()

