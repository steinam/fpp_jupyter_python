## Zeitmessung mit Python: Wie lange braucht mein Code?

from datetime import datetime

def inner():
    s = 0
    for i in range(0, 10000000):
        s = s + 1
    print(s)

start = datetime.now()

inner()

end = datetime.now()

print(end - start)

## Zeitmessung mit Jupyter (`%%timeit`)

%%timeit -n3 -r10

s = 0
for i in range(0, 10000000):
    s = s + 1
print(s)

