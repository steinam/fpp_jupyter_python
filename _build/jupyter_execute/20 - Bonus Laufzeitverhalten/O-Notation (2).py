## Die O-Notation

### Laufzeit: O(1)

def f(n):
    print("Hallo Welt")
    print("Hallo Welt")
    print("Hallo Welt")
    
f(10)

### Laufzeit: O(n)

def f(n):
    for i in range(0, n):
        print("Hallo Welt")
        print("Hallo Welt")
    
f(4)

### Laufzeit: O(n)

def f(n):
    print("Hallo Welt")
    print("Hallo Welt")
    for i in range(0, n):
        print("Hallo Welt")
    
f(4)

import matplotlib.pyplot as plt
import numpy as np

%matplotlib inline

plt.plot(np.arange(0, 25000), np.arange(0, 25000), label="n")
plt.plot(np.arange(0, 25000), np.arange(0, 25000) + 2, label="n + 2")
plt.legend()
plt.show()

### Laufzeit: O(n^2)

def f(n):
    for i in range(0, n): # O(n*n)
        for j in range(0, n): # O(n)
            print("Hallo Welt") # O(1)
    
f(4)

### Laufzeit: O(n^2)

def f(n):
    for i in range(0, int(n / 2)): # O(n*n)
        for j in range(0, int(n / 2)): # O(n) 
            print("Hallo Welt") # O(1)
    
f(4)



