## Laufzeit von eigenen Programmen abschätzen

Warum ist das wichtiger als z. B. ein paar Variable mehr oder weniger zu verwenden? Warum macht das mehr aus, als die Wahl der Programmiersprache?

- Die Wahl einer schnelleren Programmiersprache kann definitiv dein Programm beschleunigen.
- Allerdings: Viel wichtiger ist die Wahl der richtigen Datenstruktur. Das macht wirklich sehr viel mehr aus!

https://wiki.python.org/moin/TimeComplexity

### Beispiel 1: Alle Studierenden ausgeben

def f(students): # O(n)
    for student in students:
        print(student)
    
s = ["Max", "Erik", "Monika", "Tobias"]
f(s)

### Beispiel 2: Alle Studierenden sortieren und anschließend ausgeben

def f(students): # O(n*log(n))
    students.sort() # O(n*log(n))
    for student in students: # O(n)
        print(student)
    
s = ["Max", "Erik", "Monika", "Tobias"]
f(s)

### Beispiel 3: Überprüfen, ob ein Studierender bei uns eingeschrieben ist (Liste):

def f(students): # O(n)
    print("Erik" in students) # O(n)
    
s = ["Max", "Erik", "Monika", "Tobias"]
f(s)

### Beispiel 4: Überprüfen, ob ein Studierender bei uns eingeschrieben ist (Set):

def f(students): # O(1)
    print("Erik" in students) # O(1)
    
s = {"Max", "Erik", "Monika", "Tobias"}
f(s)

# Dr. Oliver Vornberger, Algorithmen und Datenstrukuren

