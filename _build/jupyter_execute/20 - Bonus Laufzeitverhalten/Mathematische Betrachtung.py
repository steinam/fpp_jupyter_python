## Mathematische Betrachtung (O-Notation)

Wenn wir eine Konstante $C$ finden können, so dass für ein genügend großes $n$ $g(n)$ nie größer als $C * f(n)$ ist, dann ist $g(n)$ in $f(n)$.

Beispiel: $g(n) = 10n^2 + 2n + 4$

*Frage:*  Ist $g(n)$ in $O(n)$?

- Wir setzen also $f(n)$ auf $n$
- Gibt es jetzt ein $C$, so dass (ab einem gewissen n) gilt: $g(n) <= C * f(n)$, also $10n^2 + 2n + 4 <= C * n$?

Da gibt es kein C.



14*