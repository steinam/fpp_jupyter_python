### Hinführung: O-Notation

%%timeit -n1 -r1

def f():
    s = 0
    for i in range(0, 1000000):
        s = s + 1
    print(s)
f()

### Idee: Wir ignorieren konkrete Laufzeiten

import matplotlib.pyplot as plt
import numpy as np

%matplotlib inline

plt.plot(np.arange(1, 600), np.arange(1, 600), label="f(n) = n")
plt.plot(np.arange(1, 600), np.arange(1, 600) * 2, label="f(n) = 2n")
plt.legend()

plt.xlabel("n")
plt.ylabel("Anzahl print() - Befehle")
plt.show()


plt.plot(np.arange(1, 6), np.repeat(100, 5), label="f(n) = 100")
plt.plot(np.arange(1, 6), np.arange(1, 6), label="f(n) = n")
plt.legend()

plt.xlabel("n")
plt.show()

# Konstante Laufzeit vs. Lineare Laufzeit

plt.plot(np.arange(1, 600), np.repeat(100, 599), label="f(n) = 100")
plt.plot(np.arange(1, 600), np.arange(1, 600), label="f(n) = n")
plt.legend()

plt.xlabel("n")
plt.show()

# Konstante Laufzeit vs. Lineare Laufzeit

plt.plot(np.arange(1, 600), np.repeat(1, 599), label="f(n) = 1")
plt.plot(np.arange(1, 600), np.arange(1, 600), label="f(n) = n")
plt.legend()

plt.xlabel("n")
plt.show()

