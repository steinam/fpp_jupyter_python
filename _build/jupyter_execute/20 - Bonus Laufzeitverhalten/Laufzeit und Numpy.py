## Warum macht Numpy so viel Sinn?

In dieser Lektion geht es darum:

- Warum du für umfangreiche, numerische Berechnungen auf jeden Fall numpy verwenden solltest.

import numpy as np
import math

# Anzahl der Einträge, die wir auswerten sollen
N = 1000000

# Liste mit 1 Mio. Einträgen, Zufallszahlen zwischen 0 und 1 (z.B. Messwerte)
entries_np = np.random.random(N)

entries_np

### Beispiel: Wurzel berechnen mit Python (for-Schleife)

entries = list(entries_np)

%%timeit -n3 -r10
out = []
for entry in entries:
    out.append(math.sqrt(entry)) 

### Beispiel: Wurzel berechnen mit Python (List-Comprehension)

%%timeit -n3 -r10
out = [math.sqrt(entry) for entry in entries]

### Beispiel: Wurzel berechnen mit Numpy

%%timeit -n3 -r100
out = np.sqrt(entries_np)

Weitere Informationen:

- Suchen nach: "Vectorization numpy"
- SSE: https://de.wikipedia.org/wiki/Streaming_SIMD_Extensions



